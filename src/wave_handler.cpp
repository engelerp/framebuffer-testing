#include <wave_handler.hpp>
#include <toolbox.hpp>
#include <vector>
#include <glad/glad.h>
#include <iostream>
#include <string>
#include <shader.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <fstream>

int ij_to_tex(int i, int j, int texwidth) {
  return 4 * i * texwidth + 4 * j;
}

WaveHandler::WaveHandler(Toolbox& tb)
  : width_(tb.screen_w), height_(tb.screen_h), 
    texwidth_(tb.texture_w), texheight_(tb.texture_h),
    texoffset_left_(tb.texoffset_left), texoffset_right_(tb.texoffset_right),
    texoffset_bottom_(tb.texoffset_bottom), texoffset_top_(tb.texoffset_top),
    shader_path_(tb.shader_path),
    vao_2d_(0), vbo_2d_(0), ebo_2d_(0), num_elements_2d_(0),
    vao_3d_(0), vbo_3d_(0), ebo_3d_(0), num_elements_3d_(0), tex_3d_checkerboard_(0),
    vao_render_(0), vbo_render_(0), ebo_render_(0), num_elements_render_(0),
    fb_comp_0_(0), fb_comp_1_(0), fb_comp_damp_(0), tex_comp_wave_0_(0), tex_comp_wave_1_(0),
    tex_comp_damp_(0), tex_comp_damp_dynamic_(0), tex_comp_damp_static_(0),
    tex_palette_(0)
{
}

/*OpenGL must be loaded before this call (i.e. infra.init() must have executed)*/
bool WaveHandler::initialize(const std::string damping_file, const std::string palette_file)
{
  bool retBool = true;
  retBool &= initialize_2D_data_();
  //retBool &= initialize_3D_data_();
  retBool &= initialize_comp_data_(damping_file);
  retBool &= initialize_render_data_(palette_file);
  retBool &= initialize_shaders_();
  return retBool;
}

void WaveHandler::update(Toolbox& tb) {
  /*Check the mailbox*/
  for (Message& m : tb.mailbox) {
    if (m.target == MESSAGETARGET::WAVE) {
      WAVEMESSAGE message = std::get<WAVEMESSAGE>(m.message);
      switch (message) {
        /*TODO: If we encounter performance issues here, we can think about using glCopyTexSubImage2D*/
      case WAVEMESSAGE::RESET_WAVE:
        tb.time = 0.f;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex_comp_wave_0_);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth_, texheight_, GL_RGBA, GL_FLOAT, tex_wave_data_.data());
        glBindTexture(GL_TEXTURE_2D, tex_comp_wave_1_);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth_, texheight_, GL_RGBA, GL_FLOAT, tex_wave_data_.data());
        glBindTexture(GL_TEXTURE_2D, 0);
        break;
      case WAVEMESSAGE::RESET_DAMPING:
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex_comp_damp_);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth_, texheight_, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
        glBindTexture(GL_TEXTURE_2D, tex_comp_damp_dynamic_);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth_, texheight_, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
        glBindTexture(GL_TEXTURE_2D, tex_comp_damp_static_);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth_, texheight_, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
        glBindTexture(GL_TEXTURE_2D, 0);
        break;
      default:
        break;
      }
    }
  }
}

void WaveHandler::prepare_step() {
  /*combine damping textures here*/
  glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_damp_);
  glViewport(0, 0, texwidth_, texheight_);
  /*Maybe we need to glClear here*/
  shdr_damp_.use();
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_static_);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_dynamic_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_2d_);
  glBindVertexArray(vao_2d_);
  glDrawElements(GL_TRIANGLES, num_elements_2d_, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool WaveHandler::step(Toolbox& tb, const int num_dsteps)
{
  if (tb.g_state == static_cast<int>(GSTATE::FREEZE)) {
    return false;
  }
  else {
    /*Perform num_dsteps doublesteps (1 doublestep = render back and forth)*/
    for (int i = 0; i < num_dsteps; ++i) {
      //render to framebuffer 0
      glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_0_);
      glViewport(0, 0, texwidth_, texheight_);

      /*Debugging*/
      /*
      std::vector<float> pixel_vector(4);
      glReadPixels(texwidth_ / 2, texheight_ / 2, 1, 1, GL_RGBA, GL_FLOAT, &pixel_vector[0]);
      std::cout << "Pixel: " << pixel_vector[0] << " " << pixel_vector[1] << " " << pixel_vector[2] << " " << pixel_vector[3] << std::endl;
      */
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      shdr_step_.use();
      shdr_step_.setFloat("t", tb.time);
      shdr_step_.setFloat("amplitude", tb.source_amplitude);
      shdr_step_.setFloat("frequency", tb.source_frequency);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, tex_comp_wave_1_);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, tex_comp_damp_);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_2d_);
      glBindVertexArray(vao_2d_);
      glDrawElements(GL_TRIANGLES, num_elements_2d_, GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
      tb.time += tb.dt;

      //swap framebuffers, repeat
      glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_1_);
      glViewport(0, 0, texwidth_, texheight_);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      shdr_step_.use();
      shdr_step_.setFloat("t", tb.time);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, tex_comp_wave_0_);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, tex_comp_damp_);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_2d_);
      glBindVertexArray(vao_2d_);
      glDrawElements(GL_TRIANGLES, num_elements_2d_, GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      tb.time += tb.dt;
    }

    return true;

  }
}

//TODO: This only does 2D so far
bool WaveHandler::render()
{
  glViewport(0, 0, width_, height_);
  glClearColor(0.1f, 0.1f, 0.2f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
  shdr_2d_.use();
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_comp_wave_1_);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_1D, tex_palette_);
  //glBindTexture(GL_TEXTURE_2D, tex_3d_checkerboard_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_render_);
  glBindVertexArray(vao_render_);
  glEnable(GL_MULTISAMPLE);
  glDrawElements(GL_TRIANGLES, num_elements_render_, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  return true;
}

GLuint WaveHandler::get_damping_tex() const
{
  return tex_comp_damp_;
}

unsigned WaveHandler::get_width() const
{
  return width_;
}

unsigned WaveHandler::get_height() const
{
  return height_;
}

bool WaveHandler::initialize_2D_data_()
{
  //vertices
  std::vector<float> pos_uv = {// x     y     z     U     V
                                  1.f,  1.f,  0.f,  1.f,  1.f, //top right
                                  1.f,  -1.f, 0.f,  1.f,  0.f, //bottom right
                                  -1.f, -1.f, 0.f,  0.f,  0.f, //bottom left
                                  -1.f, 1.f,  0.f,  0.f,  1.f }; //top left
  //elements
  std::vector<GLuint> elements = { 0, 1, 2, 2, 3, 0 };
  num_elements_2d_ = elements.size();

  //generate and initialize vao correctly, with vbo and ebo
  glGenVertexArrays(1, &vao_2d_);
  glGenBuffers(1, &vbo_2d_);
  glGenBuffers(1, &ebo_2d_);
  glBindVertexArray(vao_2d_);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_2d_);
  glBufferData(GL_ARRAY_BUFFER, pos_uv.size() * sizeof(float), pos_uv.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_2d_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);
  
  return true;
}

bool WaveHandler::initialize_3D_data_()
{
	//create checkerboard texture data
  std::vector<float> texture_data;
  texture_data.reserve(4 * width_ * height_);
  for (size_t i = 0; i < height_; ++i) {
    for (size_t j = 0; j < width_; ++j) {
      float colour = 0.f;
      //both are on an odd or even patch
      if ((i / 50 & 1) == (j / 50 & 1)) {
        colour = 1.f;
      }
      texture_data.push_back(colour);
      texture_data.push_back(colour);
      texture_data.push_back(colour);
      texture_data.push_back(1.);
    }
  }
  //create GL texture and upload checkerboard texture data
  glGenTextures(1, &tex_3d_checkerboard_);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_3d_checkerboard_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width_, height_, 0, GL_RGBA, GL_FLOAT, texture_data.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);

  //create vertex data
  std::vector<float> pos_uv;
  pos_uv.reserve(5 * width_ * height_);
  for (size_t i = 0; i < width_; ++i) {
    for (size_t j = 0; j < height_; ++j) {
      pos_uv.push_back(2.f * i / static_cast<float>(width_) - 1.f);
      pos_uv.push_back(2.f * j / static_cast<float>(height_) - 1.f);
      pos_uv.push_back(0.f);
      pos_uv.push_back(i / static_cast<float>(width_));
      pos_uv.push_back(j / static_cast<float>(height_));
    }
  }
  //create element indices (triangle vertex indices)
  std::vector<GLuint> elements;
  elements.reserve(6 * (pos_uv.size() / 5 - width_ - height_ + 1));
  for (size_t i = 0; i < width_ - 1; ++i) {
    for (size_t j = 0; j < height_ - 1; ++j) {
      size_t base_index = height_ * i + j;
      elements.push_back(base_index);
      elements.push_back(base_index + height_ + 1);
      elements.push_back(base_index + height_);
      elements.push_back(base_index);
      elements.push_back(base_index + 1);
      elements.push_back(base_index + height_ + 1);
    }
  }
  num_elements_3d_ = elements.size();
  //generate and initialize vao correctly, with vbo and ebo
  glGenVertexArrays(1, &vao_3d_);
  glGenBuffers(1, &vbo_3d_);
  glGenBuffers(1, &ebo_3d_);
  glBindVertexArray(vao_3d_);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_3d_);
  glBufferData(GL_ARRAY_BUFFER, pos_uv.size() * sizeof(float), pos_uv.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_3d_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);

  return true;
}

bool WaveHandler::initialize_comp_data_(const std::string damping_file)
{
  /*If we can load damping externally, we do that. Else we generate it ourselves*/
  std::cout << "Loading baseline damping texture" << std::endl;
  if (!load_damping_(damping_file)) {
    std::cout << "Load failed, generating fallback" << std::endl;
    int tapered_pixels = 280;
    float delta_damping = 0.00005f;
    float high_damping = 0.985f;
    float low_damping = 0.999f;
    tex_damp_data_ = std::vector<float>(4 * texheight_ * texwidth_, high_damping);
    unsigned offset = texoffset_bottom_ * texwidth_ * 4 + texoffset_left_ * 4;
    for (size_t i = 0; i < height_; ++i) {
      for (size_t j = 0; j < width_; ++j) {
        tex_damp_data_[offset + 4 * texwidth_ * i + 4 * j] = low_damping;
        tex_damp_data_[offset + 4 * texwidth_ * i + 4 * j + 1] = 0.f;
        tex_damp_data_[offset + 4 * texwidth_ * i + 4 * j + 2] = 0.f;
        tex_damp_data_[offset + 4 * texwidth_ * i + 4 * j + 3] = 0.f;
      }
    }
  }
  else {
#ifndef NDEBUG
    std::cout << "Damping " + damping_file + " loaded" << std::endl;
#endif
#ifdef NDEBUG
    std::cout << "Load successful" << std::endl;
#endif
  }
  //generate texture and upload to GPU
  glGenTextures(1, &tex_comp_damp_);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);
  //also upload to the other two damping textures
  glGenTextures(1, &tex_comp_damp_dynamic_);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_dynamic_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);
  glGenTextures(1, &tex_comp_damp_static_);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex_comp_damp_static_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);



  //generate wave data texture data
  int source_offset = 0; /*used to be 50*/
  //std::vector<float> texData(4 * height_ * width_, 0.f); //rgba
  tex_wave_data_ = std::vector<float>(4 * texheight_ * texwidth_, 0.f);
  for (size_t i = 0; i < tex_wave_data_.size() / 4; ++i) {
    tex_wave_data_[4 * i + 3] = 1.f; //alpha
  }
  //Wide source
  for (int i = 0; i < texheight_; ++i) {
    tex_wave_data_[4 * i * texwidth_ + 2] = 1.f;
  }

  //framebuffer 0
  glGenFramebuffers(1, &fb_comp_0_);
  glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_0_);
  glGenTextures(1, &tex_comp_wave_0_);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_comp_wave_0_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_wave_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_comp_wave_0_, 0);
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    std::cout << "ERROR::FRAMEBUFFER: Framebuffer 0 not complete!" << std::endl;
    return false;
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  //framebuffer 1
  glGenFramebuffers(1, &fb_comp_1_);
  glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_1_);
  glGenTextures(1, &tex_comp_wave_1_);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_comp_wave_1_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_wave_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_comp_wave_1_, 0);
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    std::cout << "ERROR::FRAMEBUFFER: Framebuffer 1 not complete!" << std::endl;
    return false;
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  //damping framebuffer
  glGenFramebuffers(1, &fb_comp_damp_);
  glBindFramebuffer(GL_FRAMEBUFFER, fb_comp_damp_);
  glActiveTexture(GL_TEXTURE0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_comp_damp_, 0);
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    std::cout << "ERROR::FRAMEBUFFER: Framebuffer 1 not complete!" << std::endl;
    return false;
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return true;
}

bool WaveHandler::initialize_render_data_(const std::string palette_file) {
  /*load colour palette*/
  std::cout << "Loading palette" << std::endl;
  if (!load_palette_(palette_file)) {
    return false;
  }
  /*generate texture and upload data*/
  glGenTextures(1, &tex_palette_);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_1D, tex_palette_);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, palette_.size()/4, 0, GL_RGBA, GL_FLOAT, palette_.data());
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_1D, 0);

  //vertices
  /*TODO: calculate min and max for U and V here*/
  float umin = static_cast<float>(texoffset_left_) / static_cast<float>(texwidth_);
  float umax = static_cast<float>(texwidth_ - texoffset_right_) / static_cast<float>(texwidth_);
  float vmin = static_cast<float>(texoffset_bottom_) / static_cast<float>(texheight_);
  float vmax = static_cast<float>(texheight_ - texoffset_top_) / static_cast<float>(texheight_);
  
  
  std::vector<float> pos_uv = {// x     y     z     U     V
                                  1.f,  1.f,  0.f,  umax,  vmax, //top right
                                  1.f,  -1.f, 0.f,  umax,  vmin, //bottom right
                                  -1.f, -1.f, 0.f,  umin,  vmin, //bottom left
                                  -1.f, 1.f,  0.f,  umin,  vmax }; //top left
  
  /*
  std::vector<float> pos_uv = {// x     y     z     U     V
                                  1.f,  1.f,  0.f,  1.f,  1.f, //top right
                                  1.f,  -1.f, 0.f,  1.f,  0.f, //bottom right
                                  -1.f, -1.f, 0.f,  0.f,  0.f, //bottom left
                                  -1.f, 1.f,  0.f,  0.f,  1.f }; //top left
  */
  //elements
  std::vector<GLuint> elements = { 0, 1, 2, 2, 3, 0 };
  num_elements_render_ = elements.size();

  glGenVertexArrays(1, &vao_render_);
  glGenBuffers(1, &vbo_render_);
  glGenBuffers(1, &ebo_render_);
  glBindVertexArray(vao_render_);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_render_);
  glBufferData(GL_ARRAY_BUFFER, pos_uv.size() * sizeof(float), pos_uv.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_render_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);

  return true;
}

bool WaveHandler::initialize_shaders_()
{
  //set up stepper shader
  Shader stepshader((shader_path_ + "stepwave.vert").c_str(), (shader_path_ + "stepwave.frag").c_str());
  shdr_step_ = stepshader;
  shdr_step_.use();
  shdr_step_.setFloat("dx", 1.f / static_cast<float>(texwidth_));
  shdr_step_.setFloat("dy", 1.f / static_cast<float>(texheight_));
  //shdr_step_.setFloat("c1", 0.4096);
  //shdr_step_.setFloat("c2", 0.3616);
  shdr_step_.setFloat("c1", 0.462393);
  shdr_step_.setFloat("c2", 0.150428);
  shdr_step_.setInt("tex_wave", 0);
  shdr_step_.setInt("tex_damp", 1);

  /*
  //set up 3d rendering shader
  Shader render3dshader((shader_path_ + "render3d.vert").c_str(), (shader_path_ + "render3d.frag").c_str());
  shdr_3d_ = render3dshader;
  shdr_3d_.use();
  shdr_3d_.setInt("tex_wave", 0);
  shdr_3d_.setInt("tex_checkerboard", 1);
  shdr_3d_.setFloat("uv_offscreen", uv_off_screen_);
  //view transformation matrix
  glm::mat4 trans_3d = glm::mat4(1.0f);
  trans_3d = glm::rotate(trans_3d, glm::radians(60.f), glm::vec3(-1.0f, 0.0f, 0.0f));
  shdr_3d_.setMat4("trans", trans_3d);
  */

  /*
  //set up 3d debug rendering shader
  Shader render3ddebugshader((shader_path_ + "render3d.vert").c_str(), (shader_path_ + "render3d.frag").c_str());
  shdr_3d_dbg_ = render3ddebugshader;
  shdr_3d_dbg_.use();
  shdr_3d_dbg_.setInt("tex_wave", 0);
  shdr_3d_dbg_.setInt("tex_checkerboard", 1);
  shdr_3d_dbg_.setFloat("uv_offscreen", 0.f);
  //view transformation matrix
  glm::mat4 trans_3d_dbg = glm::mat4(1.0f);
  trans_3d_dbg = glm::rotate(trans_3d_dbg, glm::radians(60.f), glm::vec3(-1.0f, 0.0f, 0.0f));
  shdr_3d_.setMat4("trans", trans_3d_dbg);
  */

  //set up 2d rendering shader
  Shader render2dshader((shader_path_ + "render2d.vert").c_str(), (shader_path_ + "render2d.frag").c_str());
  shdr_2d_ = render2dshader;
  shdr_2d_.use();
  shdr_2d_.setInt("tex_wave", 0);
  shdr_2d_.setInt("tex_damp", 1);
  shdr_2d_.setInt("tex_palette", 2);

  //set up 2d debug rendering shader
  Shader render2ddebugshader((shader_path_ + "render2d.vert").c_str(), (shader_path_ + "render2d.frag").c_str());
  shdr_2d_dbg_ = render2ddebugshader;
  shdr_2d_dbg_.use();
  shdr_2d_dbg_.setInt("tex_wave", 0);
  shdr_2d_dbg_.setInt("tex_damp", 1);
  shdr_2d_dbg_.setFloat("uv_offscreen", 0.f);

  //set up damping combiner shader
  Shader dampingcombinershader((shader_path_ + "combine_damping.vert").c_str(), (shader_path_ + "combine_damping.frag").c_str());
  shdr_damp_ = dampingcombinershader;
  shdr_damp_.use();
  shdr_damp_.setInt("tex_damping_static", 0);
  shdr_damp_.setInt("tex_damping_dynamic", 1);

  return true;
}

void WaveHandler::generate_and_transfer_textures(Toolbox& tb) const {
  /*Generate vanilla damping texture*/
  glGenTextures(1, &tb.tex_damp_clean);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tb.tex_damp_clean);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_damp_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);

  /*Generate vanilla wave texture*/
  glGenTextures(1, &tb.tex_wave_clean);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tb.tex_wave_clean);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, tex_wave_data_.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);

  /*Generate constant 0 texture*/
  std::vector<float> zero_data (4 * texheight_ * texwidth_, 0.f);
  glGenTextures(1, &tb.tex_const_zero);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tb.tex_const_zero);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, texwidth_, texheight_, 0, GL_RGBA, GL_FLOAT, zero_data.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);

  /*Transfer textures*/
  tb.tex_damp_dynamic = tex_comp_damp_dynamic_;
  tb.tex_damp_static = tex_comp_damp_static_;
  tb.tex_wave_0 = tex_comp_wave_0_;
  tb.tex_wave_1 = tex_comp_wave_1_;
}

bool WaveHandler::load_damping_(const std::string filename) {
  /*Load info about the texture from configuration file*/
  int screen_width, screen_height, texture_width, texture_height;
  int texoffset_left, texoffset_right, texoffset_bottom, texoffset_top;
  std::string config_filename = filename + ".conf";
  std::fstream cfg_file;
  cfg_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    cfg_file.open(config_filename);
    cfg_file >> screen_width;
    cfg_file >> screen_height;
    cfg_file >> texture_width;
    cfg_file >> texture_height;
    cfg_file >> texoffset_left;
    cfg_file >> texoffset_right;
    cfg_file >> texoffset_bottom;
    cfg_file >> texoffset_top;
    cfg_file.close();
  }
  catch(std::ifstream::failure e){
    std::cout << "Failed to open file " + config_filename << std::endl;
    return false;
  }
#ifndef NDEBUG
  std::cout << "Loaded configuration from " + config_filename;
  std::cout << "\n\tScreen Resolution: " << screen_width << "x" << screen_height;
  std::cout << "\n\tTexture Resolution: " << texture_width << "x" << texture_height;
  std::cout << "\n\tOffset Left: " << texoffset_left;
  std::cout << "\n\tOffset Right: " << texoffset_right;
  std::cout << "\n\tOffset Bottom: " << texoffset_bottom;
  std::cout << "\n\tOffset Top: " << texoffset_top << std::endl;
#endif
  /*Check if configuration is compatible with toolbox settings*/
  if (height_ != screen_height || width_ != screen_width) {
    std::cout << "Incompatible Toolbox Screen Resolution: " << width_ << "x" << height_ << std::endl;
    return false;
  }
  else if (texheight_ != texture_height || texwidth_ != texture_width) {
    std::cout << "Incompatible Toolbox Texture Resolution: " << texwidth_ << "x" << texheight_ << std::endl;
    return false;
  }
  else if (texoffset_left_ != texoffset_left || texoffset_right_ != texoffset_right || texoffset_bottom_ != texoffset_bottom || texoffset_top_ != texoffset_top) {
    std::cout << "Incompatible Offsets" << std::endl;
    return false;
  }
#ifndef NDEBUG
  else {
    std::cout << "Texture is compatible" << std::endl;
  }
  std::cout << "Loading Texture" << std::endl;
#endif
  std::fstream dmp_file;
  std::string damping_filename = filename + ".texture";
  dmp_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    dmp_file.open(damping_filename);

    float value;
    tex_damp_data_.clear();
    tex_damp_data_.reserve(4 * texture_width * texture_height);
    for (size_t i = 0; i < 4 * texture_width * texture_height; ++i) {
      /*If the file is finished at this point, we're fucked*/
      if (dmp_file.peek() == EOF) {
        std::cout << "Unexpected End Of Damping File Encountered" << std::endl;
        tex_damp_data_.clear();
        return false;
      }
      dmp_file >> value;
      tex_damp_data_.push_back(value);
    }

    dmp_file.close();
  }
  catch (std::ifstream::failure e) {
    std::cout << "Failed to open file " + damping_filename << std::endl;
    return false;
  }

  return true;
}

bool WaveHandler::load_palette_(const std::string filename) {
  int num_colors;
  std::string config_filename = filename + ".conf";
  std::fstream cfg_file;
  cfg_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    cfg_file.open(config_filename);
    cfg_file >> num_colors;
    cfg_file.close();
  }
  catch (std::ifstream::failure e) {
    std::cout << "Failed to open file " + config_filename << std::endl;
    return false;
  }

  std::fstream col_file;
  std::string palette_filename = filename + ".texture";
  col_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    col_file.open(palette_filename);

    float value;
    palette_.clear();
    palette_.reserve(num_colors * 4);
    for (size_t i = 0; i < 4 * num_colors; ++i) {
      /*If the file is finished at this point, we're fucked*/
      if (col_file.peek() == EOF) {
        std::cout << "Unexpected End Of Palette File Encountered" << std::endl;
        return false;
      }
      col_file >> value;
      palette_.push_back(value);
    }

    col_file.close();
  }
  catch (std::ifstream::failure e) {
    std::cout << "Failed to open file " + palette_filename << std::endl;
    return false;
  }

  return true;
}