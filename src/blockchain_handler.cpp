#include <blockchain_handler.hpp>
#include <toolbox.hpp>
#include <message.hpp>
#include <enums.hpp>
#include <algorithm>
#include <iostream>

void BlockchainHandler::update(Toolbox& tb) {
	bool reload_all = false; /*Do we need to re-upload all of the blocks?*/
	/*Check if mstate changed*/
	if (previous_mstate_ != tb.m_state) {
		/*Clear dragging pairs*/
		dragpairs_.clear();
	}
	previous_mstate_ = tb.m_state;

	/*Handle Messages*/
	/*If we received a Message, we ignore all events*/
	bool ignore_events = false;
	for (Message& m : tb.mailbox) {
		if (m.target == MESSAGETARGET::BLOCKCHAIN) {
			BLOCKCHAINMESSAGE message = std::get<BLOCKCHAINMESSAGE>(m.message);
			switch (message) {
			case BLOCKCHAINMESSAGE::CLEAR:
				ignore_events = true;
				/*TODO: Clear Dragging Pairs and request removal of all Blocks here*/
				clear_blocks_();
				m.handled = true;
				break;
			case BLOCKCHAINMESSAGE::PATTERN_SINGLESLIT:
				ignore_events = true;
				/*TODO: Clear Blocks and place singleslit here*/
				clear_blocks_();
				m.handled = true;
				break;
			case BLOCKCHAINMESSAGE::PATTERN_DOUBLESLIT:
				ignore_events = true;
				/*TODO: Clear Blocks and place doubleslit here*/
				clear_blocks_();
				m.handled = true;
				break;
			case BLOCKCHAINMESSAGE::PATTERN_LATTICE:
				ignore_events = true;
				/*TODO: Clear Blocks and place lattice here*/
				clear_blocks_();
				m.handled = true;
				break;
			case BLOCKCHAINMESSAGE::PATTERN_WAVEGUIDE:
				ignore_events = true;
				/*TODO: Clear Blocks and place waveguide here*/
				clear_blocks_();
				m.handled = true;
				break;
			default:
				break;
			}
		}
	}

	/*Handle Events if we don't ignore them and the MSTATE is MSTATE::PLACE, MSTATE::DELETE or MSTATE::MOVE*/
	if (!ignore_events && (tb.m_state == static_cast<int>(MSTATE::PLACE) || tb.m_state == static_cast<int>(MSTATE::DELETE) || tb.m_state == static_cast<int>(MSTATE::MOVE) || tb.m_state == static_cast<int>(MSTATE::IMMEDIATE))) {
		for (Pevent& pev : tb.events) {
			/*TODO: We assume all events are within the simulation region*/
			/*Handle PEVENTTYPE::DOWN Events*/
			if (pev.type == PEVENTTYPE::DOWN) {
				/*Handle MSTATE::IMMEDIATE case*/
				if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
					std::cout << "Immediate placement" << std::endl;
					/*Add new block, register it to dragging list here*/
					blockchain_.push_back(Block(tb.block_width, tb.block_height, pev.itcoord_x - tb.block_width / 2, tb.texture_h - (pev.itcoord_y + tb.block_height / 2), tb.tex_damp_dynamic));
					dragpairs_.push_back({ &blockchain_.back(), pev.finger_id });
				}
				/*Handle MSTATE::PLACE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::PLACE)) {
					/*Add a new Block*/
					std::cout << "Pushing Block:" << std::endl;
					std::cout << "\t" << "Width: " << tb.block_width << std::endl;
					std::cout << "\t" << "Height: " << tb.block_height << std::endl;
					std::cout << "\t" << "x: " << pev.itcoord_x - tb.block_width / 2 << std::endl;
					std::cout << "\t" << "y: " << tb.texture_h - (pev.itcoord_y - tb.block_height / 2) << std::endl;
					std::cout << "\t" << "Texture Width: " << tb.texture_w << std::endl;
					std::cout << "\t" << "Texture Height: " << tb.texture_h << std::endl;
					std::cout << "Blockchain length: " << blockchain_.size() << std::endl;
					std::cout << "Mouse x: " << pev.itcoord_x << " / " << pev.fscoord_x << std::endl;
					std::cout << "Mouse y: " << pev.itcoord_y << " / " << pev.fscoord_y << std::endl;
					blockchain_.push_back(Block(tb.block_width, tb.block_height, pev.itcoord_x - tb.block_width / 2, tb.texture_h - (pev.itcoord_y + tb.block_height / 2), tb.tex_damp_static));
				}
				/*Handle MSTATE::DELETE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::DELETE)) {
					/*If Event is within a Block, remove Block*/
					for (Block& b : blockchain_) {
						if (b.is_inside(pev.itcoord_x, tb.texture_h - pev.itcoord_y)) {
							b.request_removal();
							//reload_all = true;
							break; //only remove one Block per request
						}
					}
				}
				/*Handle MSTATE::MOVE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::MOVE)) {
					/*If Event is within an undragged Block, add that Block with touch_id to drag_pairs*/
					for (Block& b : blockchain_) {
						if (b.is_inside(pev.itcoord_x, tb.texture_h - pev.itcoord_y)) {
							bool already_dragged = false;
							for (std::pair<Block*, SDL_FingerID>& dp : dragpairs_) {
								if (dp.first == &b) {
									already_dragged = true;
									break;
								}
							}
							if (!already_dragged) {
								/*Transfer the block from static to dynamic texture*/
								b.switch_texture(tb.tex_damp_dynamic);
								/*Add block to dragged list*/
								dragpairs_.push_back({ &b, pev.finger_id });
								break; //only one Block can be dragged per finger
							}
						}
					}
				}
			}
			/*Handle PEVENTTYPE::UP Events*/
			else if (pev.type == PEVENTTYPE::UP) {
				/*Handle MSTATE::IMMEDIATE case*/
				if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
					std::cout << "Immediate removal" << std::endl;
					/*Find block that was attached to finger, delete it from dragging list, delete block here*/
					for (auto it = dragpairs_.begin(); it != dragpairs_.end(); ++it) {
						if (it->second == pev.finger_id) {
							it->first->request_removal();
							//reload_all = true;
						}
					}
					dragpairs_.remove_if([=](auto dragpair) {return dragpair.second == pev.finger_id; });
				}
				/*Handle MSTATE::PLACE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::PLACE)) {
					/*Do nothing*/
					continue;
				}
				/*Handle MSTATE::DELETE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::DELETE)) {
					/*Do nothing*/
					continue;
				}
				/*Handle MSTATE::MOVE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::MOVE)) {
					/*First transfer all blocks associated with this finger_id to the static texture*/
					for (auto dp : dragpairs_) {
						if (dp.second == pev.finger_id) {
							dp.first->switch_texture(tb.tex_damp_static);
						}
					}
					/*If touch_id is part of a drag_pair, delete that drag pair*/
					/*TODO: This removes all pairs that have ID = finger_id. There should only be one, though.*/
					dragpairs_.remove_if([=](auto dragpair){return dragpair.second == pev.finger_id; });
				}
			}
			/*Handle PEVENTTYPE::MOVE Events*/
			else if (pev.type == PEVENTTYPE::MOVE) {
				/*Handle MSTATE::IMMEDIATE case*/
				if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
					/*Find block that is attached to finger, move it here*/
					auto target_it = std::find_if(dragpairs_.begin(), dragpairs_.end(), [=](auto dragpair) {return dragpair.second == pev.finger_id; });
					if (target_it != dragpairs_.end()) {
						target_it->first->translate(pev.itcoord_x - target_it->first->width() / 2, tb.texture_h - (pev.itcoord_y + target_it->first->height() / 2));
						//reload_all = true;
					}
				}
				/*Handle MSTATE::PLACE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::PLACE)) {
					/*Do nothing*/
					continue;
				}
				/*Handle MSTATE::DELETE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::DELETE)) {
					/*Do nothing*/
					continue;
				}
				/*Handle MSTATE::MOVE case*/
				else if (tb.m_state == static_cast<int>(MSTATE::MOVE)) {
					/*If touch_id is part of a drag_pair, move that Block*/
					auto target_it = std::find_if(dragpairs_.begin(), dragpairs_.end(), [=](auto dragpair) {return dragpair.second == pev.finger_id; });
					if (target_it != dragpairs_.end()) {
						target_it->first->translate(pev.itcoord_x - target_it->first->width() / 2, tb.texture_h - (pev.itcoord_y + target_it->first->height() / 2));
						//reload_all = true;
					}
				}
			}
		}
	}
	/*Update Blockchain*/
	//update_blocks_(tb, reload_all);
	update_blocks_(tb, false);

	/*Push info to toolbox developer region*/
	tb.num_blocks = num_blocks();
}

size_t BlockchainHandler::num_blocks() {
	return blockchain_.size();
}

void BlockchainHandler::clear_blocks_() {
	for (Block& b : blockchain_) {
		b.request_removal();
	}
	/*Nobody can be dragged if everyone is deleted*/
	dragpairs_.clear();
}

void BlockchainHandler::update_blocks_(const Toolbox& tb, bool reload_all) {
	/*Uploading and erasing*/
	GLuint damping_texture_active = tb.tex_damp_static;
	if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
		damping_texture_active = tb.tex_damp_dynamic;
	}
	for (auto bit = blockchain_.begin(); bit != blockchain_.end(); ++bit) {
		/*We remove from both textures, just to be sure*/
		if (bit->needs_removal()) {
			bit->remove();
		}
		if (bit->needs_upload()) {
			bit->upload();
		}
	}
	/*Clean up the blockchain*/
	/* This doesn't work, because it invalidates all iterators/pointers, even in a list. See https://godbolt.org/z/GzxTExc44
	blockchain_.erase(std::remove_if(blockchain_.begin(), blockchain_.end(), [](const Block& b) {return b.needs_removal(); }), blockchain_.end());
	The following preserves iterator/pointer validity, which is what we need
	*/
	blockchain_.remove_if([=](const Block& b) { return b.needs_removal(); });
	if (reload_all) {
		for (auto b : blockchain_) {
			b.upload();
		}
	}
}