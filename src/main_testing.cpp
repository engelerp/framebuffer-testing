#include <glad/glad.h>
#include <SDL.h>
#include <toolbox.hpp>
#include <wave_handler.hpp>
#include <infrastructure.hpp>
#include <iostream>
#include <input_handler.hpp>
#include <gui_handler.hpp>
#include <slim_blockchain_handler.hpp>
#include <drawing_handler.hpp>
#include <chrono>
#include <thread>
#include <pattern_handler.hpp>
#include <timeout_handler.hpp>

/*Screen resolution*/
#define WIDTH 2560
#define HEIGHT 1440
//#define WIDTH 3840
//#define HEIGHT 2160

/*Path to resources*/

int main(int argc, char** argv) {
#ifndef NDEBUG
	std::cout << "DEBUGGING" << std::endl;
#endif

	/*Paths*/
	//Home top path
	std::string top_path = "C:\\Users\\engel\\VS_Projects\\FocusTerra\\framebuffer-testing\\ft_top\\";
	//Rocket top path
	//std::string top_path = "C:\\Users\\Executor\\Desktop\\ft_top\\";

	//Derived paths
	std::string rsc_path = top_path + "resource\\";
	std::string img_path = rsc_path + "images\\";
	std::string tex_path = rsc_path + "textures\\homes\\";
	//std::string tex_path = rsc_path + "textures\\rocket\\";
	std::string font_path = top_path + "fonts\\";
	std::string shd_path = top_path + "shaders\\";

	//Expected texture dimensions
	int tex_offscreen_left = 0;
	int tex_offscreen_right = 600;
	int tex_offscreen_bottom = 600;
	int tex_offscreen_top = 600;
	int texwidth = WIDTH + tex_offscreen_left + tex_offscreen_right;
	int texheight = HEIGHT + tex_offscreen_bottom + tex_offscreen_top;

	Toolbox tb(WIDTH, HEIGHT, texwidth, texheight, tex_offscreen_left, tex_offscreen_right, tex_offscreen_bottom, tex_offscreen_top, shd_path, tex_path, rsc_path);
	WaveHandler waves(tb);

	waves.initialize(tex_path + "bare", rsc_path + "ft_palette");
	waves.generate_and_transfer_textures(tb);
	InputHandler ioHandler;
	GuiHandler guiHandler(0.15f);
	guiHandler.init(tb, img_path, font_path);
	SlimBlockchainHandler bch(tb);
	DrawingHandler drah(tb);
	PatternHandler paha(tb);
	TimeoutHandler tiha(600);
	
	//Scale Gui
	ImGui::GetStyle().ScaleAllSizes(2.f);

	while (tb.g_state != static_cast<int>(GSTATE::EXIT)) {

		//Start of new frame
		//Clear Toolbox
		tb.newFrame();

		//I/O Handler update
		ioHandler.update(tb);

		//Timeout Handler update
		tiha.update(tb);

		//Gui Handler
		guiHandler.update(tb);

		//Waves Handler update
		waves.update(tb);

		//Blockchain Handler update
		bch.update(tb);

		//Drawing Handler update
		drah.update(tb);

		//Pattern Handler update
		paha.update(tb);

		//Wave Handler stepping
		waves.prepare_step();
		waves.step(tb, 6);

		//Wave Handler rendering
		waves.render();

		//Gui Handler rendering
		guiHandler.render(tb);

		//Swap Windows
		SDL_GL_SwapWindow(tb.infra.window());

		//Check for OpenGL errors
#ifndef NDEBUG
		GLint err = glGetError();
		if (err != GL_NO_ERROR) {
			std::cout << "Error Code: " << err << std::endl;
			std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
			std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
			std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
			std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
			std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
			std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
		}

		//Potential delay for single frame debugging
		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
#endif
	}

	return 0;
}