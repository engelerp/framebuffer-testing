#include <timeout_handler.hpp>
#include <chrono>

TimeoutHandler::TimeoutHandler(int seconds_to_timeout)
	: seconds_to_timeout_(seconds_to_timeout),
		time_last_input_(std::chrono::high_resolution_clock::now())
{}

void TimeoutHandler::update(Toolbox& tb) {
	auto now = std::chrono::high_resolution_clock::now();
	if (tb.events.size() == 0) {
		/*Reset if the program has timed out*/
		if (std::chrono::duration_cast<std::chrono::seconds>(now - time_last_input_).count() >= seconds_to_timeout_) {
			time_last_input_ = now;
			tb.source_frequency = 5.f;
			tb.g_state = static_cast<int>(GSTATE::RUN);
			tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
			tb.s_state = static_cast<int>(SSTATE::RUN);
			tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
			tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
			tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
			tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		}
	}
	else {
		time_last_input_ = now;
	}
	tb.timeout_threshold = seconds_to_timeout_;
	tb.timeout_timer = std::chrono::duration_cast<std::chrono::seconds>(now - time_last_input_).count();
}