#include <toolbox.hpp>
#include <enums.hpp>

Toolbox::Toolbox(const int screen_w, const int screen_h, //screen resolution
	const int texture_w, const int texture_h, //texture resolution
	const int texoffset_left, //texture offset left
	const int texoffset_right, //texture offset right
	const int texoffset_bottom, //texture offset bottom
	const int texoffset_top, //texture offset top
	const std::string shaderpath, //path to shaders
	const std::string texturepath, //path to textures
	const std::string resourcepath) //path to resources
	:
	mailbox(0),
	events(0),
	pevFactory(screen_w, screen_h, texture_w, texture_h, texoffset_left, texoffset_right, texoffset_bottom, texoffset_top),
	infra(),
	m_state(static_cast<int>(MSTATE::IMMEDIATE)),
	s_state(static_cast<int>(SSTATE::RUN)),
	g_state(static_cast<int>(GSTATE::RUN)),
	block_width(60),
	block_height(60),
	source_amplitude(.3f),
	source_frequency(5.f),
	screen_w(screen_w),
	screen_h(screen_h),
	texture_w(texture_w),
	texture_h(texture_h),
	texoffset_left(texoffset_left),
	texoffset_right(texoffset_right),
	texoffset_bottom(texoffset_bottom),
	texoffset_top(texoffset_top),
	shader_path(shaderpath),
	texture_path(texturepath),
	resource_path(resource_path),
	time(0.f),
	//dt(0.016f)
	dt(0.007477f)
{
	infra.init("Waves Window Title", screen_w, screen_h);
	//mailbox.reserve(50);
	//events.reserve(50);
}

void Toolbox::newFrame() {
	mailbox.clear();
	events.clear();
}