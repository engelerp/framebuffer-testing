#include <block.hpp>
#include <glad/glad.h>
#include <iostream>

Block::Block(const Block& o)
	:	w_(o.w_), h_(o.h_), xoffset_(o.xoffset_), yoffset_(o.yoffset_),
		needs_upload_(o.needs_upload_), needs_removal_(o.needs_removal_),
		texture_damp_(o.texture_damp_), texture_wave_(o.texture_wave_)
{}
Block::Block(int w, int h, int xoffset, int yoffset, GLuint texture_damp, GLuint texture_wave)
	:	w_(w), h_(h), xoffset_(xoffset), yoffset_(yoffset), needs_upload_(true), 
		needs_removal_(false), texture_damp_(texture_damp), texture_wave_(texture_wave)
{
}

bool Block::needs_upload() const {
	return needs_upload_;
}
bool Block::needs_removal() const {
	return needs_removal_;
}
void Block::request_removal() {
	needs_removal_ = true;
}
void Block::upload() {
	/*Damping*/
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_damp_);
	/*Handle cases where we would write out of the texture*/
	/*TODO: More cases like this can arise if we change texture geometry*/
	int effective_xoffset = xoffset_ > 0 ? xoffset_ : 0;
	int effective_width = xoffset_ > 0 ? w_ : w_ + xoffset_;
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, effective_xoffset, yoffset_, effective_xoffset, yoffset_, effective_width, h_);
	
	/*Wave*/
	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glBindTexture(GL_TEXTURE_2D, texture_wave_);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, effective_xoffset, yoffset_, effective_xoffset, yoffset_, effective_width, h_);
	glBindTexture(GL_TEXTURE_2D, 0);
	needs_upload_ = false;
}
void Block::remove() {
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_damp_);
	/*Handle cases where we would write out of the texture*/
	/*TODO: More cases like this can arise if we change texture geometry*/
	int effective_xoffset = xoffset_ > 0 ? xoffset_ : 0;
	int effective_width = xoffset_ > 0 ? w_ : w_ + xoffset_;
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, effective_xoffset, yoffset_, effective_xoffset, yoffset_, effective_width, h_);
	glBindTexture(GL_TEXTURE_2D, 0);
}
void Block::translate(int new_x, int new_y) {
	remove();
	xoffset_ = new_x;
	yoffset_ = new_y;
	upload();
}
int Block::width() const
{
	return w_;
}
int Block::height() const
{
	return h_;
}
bool Block::is_inside(int x, int y) const {
	return ((x >= xoffset_) && (x <= xoffset_ + w_) && (y >= yoffset_) && (y < yoffset_ + h_));
}
/*
Block Block::translated(int new_x, int new_y) {
	return Block(w_, h_, new_x, new_y, new_value_, reset_value_);
}
*/
int Block::xoffset() const {
	return xoffset_;
}
int Block::yoffset() const {
	return yoffset_;
}

GLuint& Block::texture_damp() {
	return texture_damp_;
}

void Block::switch_texture(GLuint new_texture) {
	remove();
	texture_damp_ = new_texture;
	upload();
}