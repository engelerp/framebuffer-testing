#include <efficient_block.hpp>

EfficientBlock::EfficientBlock(const EfficientBlock& o)
	: xywh_(o.xywh_), needs_removal_(o.needs_removal_)
{}

EfficientBlock::EfficientBlock(int w, int h, int xoffset, int yoffset)
	: xywh_(xoffset, yoffset, w, h), needs_removal_(false)
{}

void EfficientBlock::request_removal() {
	needs_removal_ = true;
}

void EfficientBlock::translate(int new_x, int new_y) {
	xywh_.r = new_x;
	xywh_.g = new_y;
}

bool EfficientBlock::needs_removal() const {
	return needs_removal_;
}

glm::ivec4 EfficientBlock::xywh() const {
	return xywh_;
}

bool EfficientBlock::is_inside(int x, int y) const {
	return ((x >= xywh_.r) && (x <= xywh_.r + xywh_.b) && (y >= xywh_.g) && (y < xywh_.g + xywh_.a));
}

int EfficientBlock::width() const {
	return xywh_.b;
}

int EfficientBlock::height() const {
	return xywh_.a;
}