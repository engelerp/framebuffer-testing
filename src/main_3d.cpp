#include <glad/glad.h>
//#include <SDL2/SDL.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <init.hpp>
#include <infrastructure.hpp>
#include <shader.hpp>
#include <chrono>
#include <vector>
#include <timer.hpp>
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>
#include <list>

#define WIDTH 2048
#define HEIGHT 1024
#define SCALE 2
#define TEXWIDTH 2048
#define TEXHEIGHT 1024
#define UPDATES_PER_FRAME 2
#define FRAME_THRESHOLD 14.6f


int main(int argc, char** argv){
  //Initialize SDL, create window and OpenGL context
  Infrastructure infra;
  infra.init("Waves", WIDTH, HEIGHT);

  //Initialize ImGui
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  //(void)io;
  ImGui::StyleColorsDark();
  ImGui_ImplSDL2_InitForOpenGL(infra.window(), infra.context());
  const char* glsl_version = "#version 330";
  ImGui_ImplOpenGL3_Init(glsl_version);

  int w_width, w_height;
  SDL_GetWindowSize(infra.window(), &w_width, &w_height);

  //-----------------------
  //Data generation
  //-----------------------
  //-----------------------
  //IDEA:
  //Framebuffer 1: t-1
  //Framebuffer 2: t
  //Framebuffer 3: t+1
  //
  //(1): use shader step.vert/step.frag to step t -> t+1
  //(2): use shader copy.vert/copy.frag twice to copy t -> t-1 and t+1 -> t
  //(3): render texture t+1 to screen
  //-----------------------

  //Texture
  //Fill in a checkerboard with 100px period
  std::vector<float> texture_data;
  texture_data.reserve(4*TEXWIDTH*TEXHEIGHT);
  for(size_t j = 0; j < TEXHEIGHT; ++j){
    for(size_t i = 0; i < TEXWIDTH; ++i){
      float colour = 0.f;
      //both are on an odd or even patch
      if((i / 50 & 1) == (j / 50 & 1)){
        colour = 1.f;
      }
      texture_data.push_back(colour);
      texture_data.push_back(colour);
      texture_data.push_back(colour);
      texture_data.push_back(1.);
    }
  }

  //upload texture
  unsigned int cb_texture;
  glGenTextures(1, &cb_texture);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, cb_texture);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texture_data.data());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glBindTexture(GL_TEXTURE_2D, 0);

  //-----------------------
  //VAO setup
  //-----------------------
  //texture attributes
  /*
  std::vector<float> pos_uv_tex = {//x      y       z       U       V
                                  1.f,    1.f,    0.f,    1.f,    1.f,
                                  1.f,    -1.f,   0.f,    1.f,    0.f,
                                  -1.f,   -1.f,   0.f,    0.f,    0.f,
                                  -1.f,   -1.f,   0.f,    0.f,    0.f,
                                  1.f,    1.f,    0.f,    1.f,    1.f,
                                  -1.f,   1.f,    0.f,    0.f,    1.f};
  */
  std::vector<float> pos_uv;
  pos_uv.reserve(5*TEXWIDTH*TEXHEIGHT);
  for(size_t i = 0; i < TEXWIDTH; ++i){
    for(size_t j = 0; j < TEXHEIGHT; ++j){
      pos_uv.push_back(2.f*i/static_cast<float>(TEXWIDTH)-1.f);
      pos_uv.push_back(2.f*j/static_cast<float>(TEXHEIGHT)-1.f);
      pos_uv.push_back(0.f);
      pos_uv.push_back(i/static_cast<float>(TEXWIDTH));
      pos_uv.push_back(j/static_cast<float>(TEXHEIGHT));
    }
  }
  std::cout << "Vertices: " << pos_uv.size() <<  " (" << pos_uv.size()*sizeof(float)/1000000. << "MB)" << std::endl;


  /*Vertex Indexing:
    TEXHEIGHT-1 2*TEXHEIGHT-1         TEXHEIGHT*(TEXWIDTH-1)+TEXHEIGHT-1
    .           .                     .
    .           .                     .
    .           .                     .
    2           TEXHEIGHT+2           TEXHEIGHT*(TEXWIDTH-1)+2
    1           TEXHEIGHT+1           TEXHEIGHT*(TEXWIDTH-1)+1
    0           TEXHEIGHT    .  .  .  TEXHEIGHT*(TEXWIDTH-1)
  */
  std::vector<unsigned> elements;
  elements.reserve(6*(pos_uv.size()/5-TEXWIDTH-TEXHEIGHT+1));
  for(size_t i = 0; i < TEXWIDTH-1; ++i){
    for(size_t j = 0; j < TEXHEIGHT-1; ++j){
      size_t base_index = TEXHEIGHT*i+j;
      elements.push_back(base_index);
      elements.push_back(base_index+TEXHEIGHT);
      elements.push_back(base_index+TEXHEIGHT+1);
      elements.push_back(base_index);
      elements.push_back(base_index+1);
      elements.push_back(base_index+TEXHEIGHT+1);
    }
  }


  unsigned int vbo, vao, ebo;
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, pos_uv.size()*sizeof(float), pos_uv.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size()*sizeof(unsigned), elements.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);



  //-----------------------
  //Shader setup
  //-----------------------
  std::string shaderPath = "C:\\Users\\engel\\VS_Projects\\FocusTerra\\framebuffer-testing\\shaders\\";

  if(argc == 2){
    std::cout << "Shader Path: " << argv[1] << std::endl;
    shaderPath = argv[1];
  }
  Shader shader_3d ((shaderPath+"shader3D.vert").c_str(), (shaderPath+"shader3D.frag").c_str());

  //set shader uniforms
  shader_3d.use();
  shader_3d.setInt("texture1",0);





  std::cout << "Setup Complete" << std::endl;


  //-----------------------
  //Game Loop
  //-----------------------

  bool is_running = true;
  SDL_Event event;
  Timer timer;
  float time = 0.f;
  int count = 0;
  //game loop
  while (is_running) {
    ++count;
    timer.start();



    //-----------------------
    //Event Handling
    //-----------------------
    while (SDL_PollEvent(&event)) {
      //Pass event to imgui
      ImGui_ImplSDL2_ProcessEvent(&event);
      //now do our own processing
      if (event.type == SDL_QUIT) {
        is_running = false;
      }
    }



    //ImGui handling
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(infra.window());
    ImGui::NewFrame();

    //Debug Window
    ImGui::SetNextWindowPos(ImVec2(10.f,10.f), ImGuiCond_Always, ImVec2(0.f,0.f));
    ImGui::SetNextWindowBgAlpha(0.35f);
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;
    ImGui::Begin("Developer", NULL, window_flags);
    ImGui::Text("Developer Window");
    ImGui::Text("3D Test");
    ImGui::Text("Texture Resolution: %u x %u", TEXWIDTH, TEXHEIGHT);
    ImGui::Text("Window Resolution: %u x %u", (int)io.DisplaySize.x,(int)io.DisplaySize.y);
    ImGui::Text("Mouse Position: (%.1f, %.1f)", io.MousePos.x, io.MousePos.y);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();




    //-----------------------
    //Data update and rendering
    //-----------------------



    // glFlush();
    // while(timer.current() < FRAME_THRESHOLD){
    //   continue;
    // }
    //fucking slow operation
    SDL_GetWindowSize(infra.window(), &w_width, &w_height);
    glViewport(0,0,w_width,w_height);
    glClearColor(0.1f,0.1f,0.2f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    //shader_copy.use();
    shader_3d.use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cb_texture);
    glBindVertexArray(vao);
    glEnable(GL_MULTISAMPLE); //anti-aliasing
    glDrawElements(GL_TRIANGLES, elements.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);




    //-----------------------
    //Imgui Rendering
    //-----------------------
    ImGui::Render();
    glViewport(0,0,(int)io.DisplaySize.x,(int)io.DisplaySize.y);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());




    //Swap buffers
    SDL_GL_SwapWindow(infra.window());
/*
    //Error checking
    std::cout << "Error Code: " << glGetError() << std::endl;
    std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
    std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
    std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
    std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
    std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
    std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
*/
  }


  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();
  infra.quit();
}
