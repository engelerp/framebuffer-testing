#include <event_logger.hpp>
#include <toolbox.hpp>
#include <string>

EventLogger::EventLogger()
{
	for (size_t i = 0; i < 16; ++i) {
		events_down[i] = {};
		events_up[i] = {};
		events_move[i] = {};
		events_other[i] = {};
	}
}

void EventLogger::push_events(Toolbox& tb) {
	for (auto pev : tb.events) {
		switch (pev.type) {
		case PEVENTTYPE::DOWN:
			events_down[(i_down++) % 16] = pev;
			break;
		case PEVENTTYPE::UP:
			events_up[(i_up++) % 16] = pev;
			break;
		case PEVENTTYPE::MOVE:
			events_move[(i_move++) % 16] = pev;
			break;
		case PEVENTTYPE::OTHER:
			events_other[(i_other++) % 16] = pev;
			break;
		default:
			break;
		}
	}
}

/*Convert Pevent to std::string*/
std::string ptos(const Pevent& pev, const Toolbox& tb) {
	std::string pev_type = "";
	std::string sdl_type = "";
	std::string pev_fscoords = "(" + std::to_string(pev.fscoord_x) + ", " + std::to_string(pev.fscoord_y) + ")";
	std::string pev_itcoords = "(" + std::to_string(pev.itcoord_x) + ", " + std::to_string(pev.itcoord_y) + ")";
	std::string sdl_coords = "(n/a, n/a)";
	switch (pev.type) {
	case PEVENTTYPE::DOWN:
		pev_type = "DOWN";
		switch (pev.event.type) {
		case SDL_MOUSEBUTTONDOWN:
			sdl_type = "SDL_MOUSEBUTTONDOWN";
			sdl_coords = "(" + std::to_string(pev.event.button.x) + ", " + std::to_string(pev.event.button.y) + ")";
			break;
		case SDL_FINGERDOWN:
			sdl_type = "SDL_FINGERDOWN";
			sdl_coords = "(" + std::to_string(pev.event.tfinger.x) + ", " + std::to_string(pev.event.tfinger.y) + ")" + ", [" + std::to_string(pev.event.tfinger.x * tb.screen_w) + ", " + std::to_string(pev.event.tfinger.y * tb.screen_h) + "]";
			break;
		default:
			sdl_type = std::to_string(pev.event.type);
			break;
		}
		break;
	case PEVENTTYPE::UP:
		pev_type = "UP";
		switch (pev.event.type) {
		case SDL_MOUSEBUTTONUP:
			sdl_type = "SDL_MOUSEBUTTONUP";
			sdl_coords = "(" + std::to_string(pev.event.button.x) + ", " + std::to_string(pev.event.button.y) + ")";
			break;
		case SDL_FINGERUP:
			sdl_type = "SDL_FINGERUP";
			sdl_coords = "(" + std::to_string(pev.event.tfinger.x) + ", " + std::to_string(pev.event.tfinger.y) + ")" + ", [" + std::to_string(pev.event.tfinger.x * tb.screen_w) + ", " + std::to_string(pev.event.tfinger.y * tb.screen_h) + "]";
			break;
		default:
			sdl_type = std::to_string(pev.event.type);
			break;
		}
		break;
	case PEVENTTYPE::MOVE:
		pev_type = "MOVE";
		switch (pev.event.type) {
		case SDL_MOUSEMOTION:
			sdl_type = "SDL_MOUSEMOTION";
			sdl_coords = "(" + std::to_string(pev.event.motion.x) + ", " + std::to_string(pev.event.motion.y) + ")";
			break;
		case SDL_FINGERMOTION:
			sdl_type = "SDL_FINGERMOTION";
			sdl_coords = "(" + std::to_string(pev.event.tfinger.x) + ", " + std::to_string(pev.event.tfinger.y) + ")" + ", [" + std::to_string(pev.event.tfinger.x * tb.screen_w) + ", " + std::to_string(pev.event.tfinger.y * tb.screen_h) + "]";
			break;
		default:
			sdl_type = std::to_string(pev.event.type);
			break;
		}
		break;
	case PEVENTTYPE::OTHER:
		pev_type = "OTHER";
		sdl_type = std::to_string(pev.event.type);
		break;
	default:
		break;
	}
	return pev_type + " " + sdl_type + " " + pev_fscoords + " " + pev_itcoords + " " + sdl_coords;
}