#include <gui_handler.hpp>
#include <toolbox.hpp>
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>
#include <enums.hpp>
#include <iostream>
#include <string>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

/*Debugging: Get VRAM info*/
#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

void GuiHandler::init(Toolbox& tb, const std::string path_img, const std::string path_ttf)
{
	tb.gui_pos = gui_pos_;

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	io_ = ImGui::GetIO();
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForOpenGL(tb.infra.window(), tb.infra.context());
	const char* glsl_version = "#version 330";
	ImGui_ImplOpenGL3_Init(glsl_version);

	std::cout << "Loading images" << std::endl;
	load_button_textures_(path_img);

	/*Load font*/
	std::string fullpath_font = path_ttf + "Cousine-Regular.ttf";
	font_ = io_.Fonts->AddFontFromFileTTF(fullpath_font.c_str(), 30.0f);
	if (font_ != NULL) {
		std::cout << "Fonts loaded" << std::endl;
	}
	else {
		std::cout << "Failed to load fonts" << std::endl;
	}
	io_.Fonts->Build();
}

void GuiHandler::update(Toolbox& tb) {
	/*Check if the application has to quit*/
	for (const Pevent& ev : tb.events) {
		if (ev.event.type == SDL_QUIT) {
			tb.g_state = static_cast<int>(GSTATE::EXIT);
			break; /*We could probably return here*/
		}
	}

#ifndef NDEBUG
	/*Debugging: Hand off events to event logger evlog*/
	evlog.push_events(tb);
#endif

	/*Check if the GUI received input*/
	SDL_Event event;
	bool have_event = false;
	/*Are we in the second step of lowering a finger?*/
	if(lowering_finger_){
		event = next_event_;
		lowering_finger_ = false;
		have_event = true;
	}
	/*Are we in the second step of raising a finger?*/
	else if (raising_finger_) {
		event.type = SDL_USEREVENT;
		raising_finger_ = false;
		have_event = true;
	}
	else {
		for (const Pevent& ev : tb.events) {
			/*Either it's the finger that is controlling the gui, or it's in the gui window and nobody is controlling it yet*/
			if (ev.finger_id == fingerID_ || (isInGuiWindow_(ev) && fingerID_ == -42)) {
				/*TODO: We need a translation layer here for touch events*/
				switch (ev.event.type) {
				case SDL_FINGERDOWN:
					/*Start finger lowering procedure. First, motion to where we are, next press*/
					fingerID_ = ev.finger_id;
					deviceID_ = ev.event.tfinger.touchId;
					next_event_ = ev.event;
					event = ev.event;
					event.type = SDL_FINGERMOTION;
					lowering_finger_ = true;
					have_event = true;
					break;
				case SDL_FINGERMOTION:
					/*Move finger, but only if it's the one controlling the gui*/
					if (ev.finger_id != fingerID_) {
						continue;
					}
					else {
						event = ev.event;
						have_event = true;
						break;
					}
				case SDL_FINGERUP:
					/*Start finger raising procedure. First release the press, then reset the mouse*/
					if (ev.finger_id != fingerID_) {
						continue;
					}
					else {
						event = ev.event;
						raising_finger_ = true;
						fingerID_ = -42;
						have_event = true;
						break;
					}
				case SDL_MOUSEBUTTONDOWN:
					/*Start finger lowering procedure. First, motion to where we are, next press*/
					fingerID_ = ev.finger_id;
					next_event_ = ev.event;
					event = ev.event;
					event.type = SDL_MOUSEMOTION;
					lowering_finger_ = true;
					have_event = true;
					break;
				case SDL_MOUSEMOTION:
					if (ev.finger_id != fingerID_) {
						continue;
					}
					else {
						event = ev.event;
						have_event = true;
						break;
					}
				case SDL_MOUSEBUTTONUP:
					if (ev.finger_id != fingerID_) {
						continue;
					}
					else {
						event = ev.event;
						have_event = true;
						raising_finger_ = true;
						fingerID_ = -42;
						break;
					}
				}
			}
		}
	}
	if (have_event) {
		ImGui_ImplSDL2_Touch_ProcessEvent(&event, tb.screen_w, tb.screen_h);
	}

	/*If the controlling finger is not currently touching, reset*/
	/*Uncomment on a touchscreen*/
	/*
	if (fingerID_ != -42) {
		if (std::find(tb.current_touchIDs.begin(), tb.current_touchIDs.end(), fingerID_) == tb.current_touchIDs.end()) {
			fingerID_ = -42;
		}
	}
	*/
	/*Draw GUI*/

	/*Draw controller window*/
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(tb.infra.window());
	ImGui::NewFrame();
	//Uncomment on a touchscreen
	//ImGui::SetMouseCursor(ImGuiMouseCursor_None);
	
	draw_gui_ft_(tb);


#ifndef NDEBUG
	/*Debugging: Developer window*/
	ImGui::Begin("Developer", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0 / double(ImGui::GetIO().Framerate), double(ImGui::GetIO().Framerate));
	GLint total_mem_kb = 0;
	glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &total_mem_kb);
	ImGui::Text("Total VRAM: %.3f MB", static_cast<float>(total_mem_kb)/1000.f);
	GLint cur_avail_mem_kb = 0;
	glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &cur_avail_mem_kb);
	ImGui::Text("Available VRAM: %.3f MB", static_cast<float>(cur_avail_mem_kb) / 1000.f);
	ImGui::Text("Used VRAM: %.3f MB", static_cast<float>(total_mem_kb-cur_avail_mem_kb) / 1000.f);
	if (ImGui::GetIO().MousePos[0] < -1000000) {
		ImGui::Text("Mouse Coordinates: (N/A, N/A)");
	}
	else {
		ImGui::Text("Mouse Coordinates: (%5.f, %5.f)", ImGui::GetIO().MousePos[0], ImGui::GetIO().MousePos[1]);
	}
	ImGui::Text("Touch Device: %d", tb.touch_device);
	ImGui::Text("Number Touch Devices: %d", SDL_GetNumTouchDevices());
	ImGui::Text("Number Touches: %d", tb.current_touchIDs.size());
	std::string touch_string = "Touch IDs: ";
	for (size_t i = 0; i < tb.current_touchIDs.size(); ++i) {
		touch_string += " " + std::to_string(tb.current_touchIDs[i]);
	}
	ImGui::Text(touch_string.c_str());
	ImGui::Text("MSTATE: %d", tb.m_state);
	ImGui::Text("GSTATE: %d", tb.g_state);
	ImGui::Text("SSTATE: %d", tb.s_state);
	ImGui::Text("GUI Controlling Finger: %d", fingerID_);
	ImGui::Text("Blockchain Length: %d", tb.num_blocks);
	ImGui::Text("Drawers: %d", tb.num_drawers);
	ImGui::Text("Events: %d", tb.events.size());
	ImGui::Text("Messages: %d", tb.mailbox.size());
	ImGui::Text("Timeout Threshold: %d s", tb.timeout_threshold);
	ImGui::Text("Timeout Timer: %d s", tb.timeout_timer);
	ImGui::End();
#endif
#ifndef NDEBUG
	/*Deep Debugging: Event Console*/
	/*
	ImGui::SetNextWindowPos(ImVec2(tb.screen_w * (0.5), 0.f), ImGuiCond_Always);
	ImGui::Begin("Events", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::NewLine();
	ImGui::Text("UP EVENTS");
	for (const Pevent& pev : evlog.events_up) {
		std::string str = ptos(pev, tb);
		ImGui::Text(str.c_str());
	}
	ImGui::Separator();
	ImGui::Text("DOWN EVENTS");
	for (const Pevent& pev : evlog.events_down) {
		std::string str = ptos(pev, tb);
		ImGui::Text(str.c_str());
	}
	ImGui::Separator();
	ImGui::Text("MOVE EVENTS");
	for (const Pevent& pev : evlog.events_move) {
		std::string str = ptos(pev, tb);
		ImGui::Text(str.c_str());
	}
	ImGui::Separator();
	ImGui::Text("OTHER EVENTS");
	for (const Pevent& pev : evlog.events_other) {
		std::string str = ptos(pev, tb);
		ImGui::Text(str.c_str());
	}
	*/
#endif
}

void GuiHandler::render(Toolbox &tb) {
	ImGui::Render();
	glViewport(0, 0, tb.screen_w, tb.screen_h);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/*This must change if position / size of GUI change*/
bool GuiHandler::isInGuiWindow_(const Pevent& pev) const
{
	return pev.fscoord_x >= 1.f - gui_pos_;
}


void GuiHandler::draw_gui_(Toolbox& tb) {
	/*Set Window Position and Size and start the Window*/
	ImGui::SetNextWindowPos(ImVec2(tb.screen_w * (1.f - gui_pos_), 0.f), ImGuiCond_Always);
	ImGui::SetNextWindowSize(ImVec2(tb.screen_w * gui_pos_, tb.screen_h), ImGuiCond_Always);
	ImGui::Begin("Controller", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar);

	/*Modes*/
	if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
		if (ImGui::ImageButton((void*)(btex_immediate_on_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_immediate_off_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		}
	}
	ImGui::Dummy(ImVec2(40, 0));
	ImGui::SameLine();
	ImGui::Text("Spielen");
	if (tb.m_state == static_cast<int>(MSTATE::DRAW)) {
		if (ImGui::ImageButton((void*)(btex_draw_on_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::DRAW);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_draw_off_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::DRAW);
		}
	}
	ImGui::SameLine();
	if (tb.m_state == static_cast<int>(MSTATE::ERASE)) {
		if (ImGui::ImageButton((void*)(btex_erase_on_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::ERASE);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_erase_off_), ImVec2(140, 140))) {
			tb.m_state = static_cast<int>(MSTATE::ERASE);
		}
	}
	ImGui::Dummy(ImVec2(40, 0));
	ImGui::SameLine();
	ImGui::Text("Zeichnen");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(100, 0));
	ImGui::SameLine();
	ImGui::Text("Radieren");
	/*
	if (ImGui::Button("IMMEDIATE", ImVec2(140, 140))) {
		tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
	}
	
	if (ImGui::Button("DRAW", ImVec2(140, 140))) {
		tb.m_state = static_cast<int>(MSTATE::DRAW);
	}
	ImGui::SameLine();
	if (ImGui::Button("ERASE", ImVec2(140, 140))) {
		tb.m_state = static_cast<int>(MSTATE::ERASE);
	}
	*/
	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();

	/*Patterns*/
	if (ImGui::ImageButton((void*)(btex_lattice_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_LATTICE });
		tb.source_frequency = 5.f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_waveguide_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_WAVEGUIDE });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(40, 0));
	ImGui::SameLine();
	ImGui::Text("Gitter");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(95, 0));
	ImGui::SameLine();
	ImGui::Text("Wellenleiter");
	if (ImGui::ImageButton((void*)(btex_singleslit_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_SINGLESLIT });
		tb.source_frequency = 5.f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_doubleslit_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_DOUBLESLIT });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(40, 0));
	ImGui::SameLine();
	ImGui::Text("Einzelspalt");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(70, 0));
	ImGui::SameLine();
	ImGui::Text("Doppelspalt");
	if (ImGui::ImageButton((void*)(btex_ssh_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_SSH });
		tb.source_frequency = 1.424f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_fresnel_off_), ImVec2(140, 140))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_FRESNEL });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(20, 0));
	ImGui::SameLine();
	ImGui::Text("Metamaterial");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(23, 0));
	ImGui::SameLine();
	ImGui::Text("Fresnellinse");
	/*
	if (ImGui::Button("LATTICE", ImVec2(140, 140))) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_LATTICE });
	}
	ImGui::SameLine();
	if (ImGui::Button("WAVEGUIDE", ImVec2(140, 140))) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_WAVEGUIDE });
	}
	if (ImGui::Button("SINGLE SLIT", ImVec2(140, 140))) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_SINGLESLIT });
	}
	ImGui::SameLine();
	if (ImGui::Button("DOUBLE SLIT", ImVec2(140, 140))) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_DOUBLESLIT });
	}
	*/
	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();

	/*Game Control*/
	if (ImGui::Button("RESET", ImVec2(140, 30))) {
		/*If any of this changes, then also change TimeoutHandler::update!*/
		tb.source_frequency = 5.f; /*TODO: If initial frequency changes, this must be adapted*/
		tb.g_state = static_cast<int>(GSTATE::RUN);
		tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		tb.s_state = static_cast<int>(SSTATE::RUN);
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
	}
	ImGui::SameLine();
	if (tb.g_state != static_cast<int>(GSTATE::FREEZE)) {
		if (ImGui::Button("STOPP", ImVec2(140, 30))) {
			tb.g_state = static_cast<int>(GSTATE::FREEZE);
		}
	}
	else {
		if (ImGui::Button("START", ImVec2(140, 30))) {
			tb.g_state = static_cast<int>(GSTATE::RUN);
		}
	}
	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();

	/*Frequency Selection*/
	ImGui::Text("1 Hz");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(70, 0));
	ImGui::SameLine();
	ImGui::Text("f");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(70, 0));
	ImGui::SameLine();
	ImGui::Text("10 Hz");
	ImGui::SliderFloat("", &tb.source_frequency, 1.424f, 10.f);
	std::vector<float> wave_1;
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(2.*3.1415*t));
	}
	ImGui::SetNextItemWidth(50);
	ImGui::PushStyleColor(ImGuiCol_FrameBg, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_PlotLines, { 169./256.,218./256.,224./256.,1. });
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(70, 0));
	ImGui::SameLine();
	ImGui::Text("L");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(70, 0));
	ImGui::SameLine();
	wave_1.clear();
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(8. * 3.1415 * t));
	}
	ImGui::SetNextItemWidth(50);
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::PopStyleColor(2);

	ImGui::End();
}

void GuiHandler::draw_gui_ft_(Toolbox& tb) {
	/*Set Window Position and Size and start the Window*/
	ImGui::SetNextWindowPos(ImVec2(tb.screen_w * (1.f - gui_pos_), 0.f), ImGuiCond_Always);
	ImGui::SetNextWindowSize(ImVec2(tb.screen_w * gui_pos_, tb.screen_h), ImGuiCond_Always);
	ImGui::Begin("Controller", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar);

	/*Top Space*/
	ImGui::Dummy(ImVec2(0, 50));

	/*Modes*/
	int space_left_normal = 35;
	int image_button_dim = 210;
	int space_left_to_center = image_button_dim / 2 + space_left_normal + 7;
	ImGui::Dummy(ImVec2(space_left_to_center, 0));
	ImGui::SameLine();
	if (tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
		if (ImGui::ImageButton((void*)(btex_immediate_on_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_immediate_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		}
	}
	ImGui::Dummy(ImVec2(space_left_to_center + image_button_dim / 5 + 7, 0));
	ImGui::SameLine();
	ImGui::Text("Spielen");
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	if (tb.m_state == static_cast<int>(MSTATE::DRAW)) {
		if (ImGui::ImageButton((void*)(btex_draw_on_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::DRAW);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_draw_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::DRAW);
		}
	}
	ImGui::SameLine();
	if (tb.m_state == static_cast<int>(MSTATE::ERASE)) {
		if (ImGui::ImageButton((void*)(btex_erase_on_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::ERASE);
		}
	}
	else {
		if (ImGui::ImageButton((void*)(btex_erase_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
			tb.m_state = static_cast<int>(MSTATE::ERASE);
		}
	}
	ImGui::Dummy(ImVec2(space_left_normal + image_button_dim / 5, 0));
	ImGui::SameLine();
	ImGui::Text("Zeichnen");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(100 - image_button_dim / 6, 0));
	ImGui::SameLine();
	ImGui::Text("Radieren");

	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();

	/*Patterns*/

	//SSH
	ImGui::Dummy(ImVec2(space_left_to_center, 0));
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_ssh_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_SSH });
		tb.source_frequency = 1.424f;
	}
	ImGui::Dummy(ImVec2(space_left_to_center + 7, 0));
	ImGui::SameLine();
	ImGui::Text("Metamaterial");

	//Lattice and Waveguide
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_lattice_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_LATTICE });
		tb.source_frequency = 5.f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_waveguide_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_WAVEGUIDE });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(space_left_normal + image_button_dim / 4 + 7, 0));
	ImGui::SameLine();
	ImGui::Text("Gitter");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(49, 0));
	ImGui::SameLine();
	ImGui::Text("Wellenleiter");

	//Doubleslit, Fresnel
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_doubleslit_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_DOUBLESLIT });
		tb.source_frequency = 5.f;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_fresnel_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_FRESNEL });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(space_left_normal + image_button_dim / 12, 0));
	ImGui::SameLine();
	ImGui::Text("Doppelspalt");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(image_button_dim / 12 + image_button_dim / 18 - 14, 0));
	ImGui::SameLine();
	ImGui::Text("Fresnellinse");

	//Singleslit
	ImGui::Dummy(ImVec2(space_left_to_center, 0));
	ImGui::SameLine();
	if (ImGui::ImageButton((void*)(btex_singleslit_off_), ImVec2(image_button_dim, image_button_dim), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1))) {
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ PATTERNMESSAGE::PATTERN_SINGLESLIT });
		tb.source_frequency = 5.f;
	}
	ImGui::Dummy(ImVec2(space_left_to_center + image_button_dim / 12, 0));
	ImGui::SameLine();
	ImGui::Text("Einzelspalt");

	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();


	/*Game Control*/
	int button_width = image_button_dim;
	//int button_height = 30;
	int button_height = 40;

	//RESET and START / STOPP
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	if (ImGui::Button("NEUSTART", ImVec2(button_width, button_height))) {
		/*If any of this changes, then also change TimeoutHandler::update!*/
		tb.source_frequency = 5.f; /*TODO: If initial frequency changes, this must be adapted*/
		tb.g_state = static_cast<int>(GSTATE::RUN);
		tb.m_state = static_cast<int>(MSTATE::IMMEDIATE);
		tb.s_state = static_cast<int>(SSTATE::RUN);
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
	}
	ImGui::SameLine();
	if (tb.g_state != static_cast<int>(GSTATE::FREEZE)) {
		if (ImGui::Button("STOPP", ImVec2(button_width, button_height))) {
			tb.g_state = static_cast<int>(GSTATE::FREEZE);
		}
	}
	else {
		ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor(247, 248, 68));
		ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(0, 0, 0));
		if (ImGui::Button("START", ImVec2(button_width, button_height))) {
			tb.g_state = static_cast<int>(GSTATE::RUN);
		}
		ImGui::PopStyleColor(2);
	}

	ImGui::NewLine();
	ImGui::Separator();
	ImGui::NewLine();


	/*Frequency Selection Slider*/
	/*
	//Old (no-wavelength) slider
	ImGui::Dummy(ImVec2(space_left_normal - 18, 0));
	ImGui::SameLine();
	ImGui::Text("1.4 Hz");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(95, 0));
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(150, 0));
	ImGui::SameLine();
	ImGui::Text("10 Hz");
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	ImGui::SetNextItemWidth(2 * image_button_dim + 15);
	ImGui::SliderFloat("", &tb.source_frequency, 1.424f, 10.f);
	std::vector<float> wave_1;
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(2. * 3.1415 * t));
	}
	ImGui::Dummy(ImVec2(space_left_normal - 18, 0));
	ImGui::SameLine();
	ImGui::SetNextItemWidth(50);
	ImGui::PushStyleColor(ImGuiCol_FrameBg, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_PlotLines, { 169. / 256.,218. / 256.,224. / 256.,1. });
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(90, 0));
	ImGui::SameLine();
	ImGui::Text("Frequenz");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(93, 0));
	ImGui::SameLine();
	wave_1.clear();
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(8. * 3.1415 * t));
	}
	ImGui::SetNextItemWidth(50);
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::PopStyleColor(2);
	*/
	//waveplots
	std::vector<float> wave_1;
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(2. * 3.1415 * t));
	}
	ImGui::Dummy(ImVec2(space_left_normal - 18, 0));
	ImGui::SameLine();
	ImGui::SetNextItemWidth(50);
	ImGui::PushStyleColor(ImGuiCol_FrameBg, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_PlotLines, { 169. / 256.,218. / 256.,224. / 256.,1. });
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(340, 0));
	ImGui::SameLine();
	wave_1.clear();
	for (float t = 0; t < 1.f; t += .05f) {
		wave_1.push_back(std::sin(8. * 3.1415 * t));
	}
	ImGui::SetNextItemWidth(50);
	ImGui::PlotLines("", wave_1.data(), wave_1.size());
	ImGui::PopStyleColor(2);
	//slider
	ImGui::Dummy(ImVec2(space_left_normal, 0));
	ImGui::SameLine();
	ImGui::SetNextItemWidth(2 * image_button_dim + 15);
	ImGui::SliderFloat("", &tb.source_frequency, 1.424f, 10.f, "%.2f Hz");
	//frequency text
	ImGui::Dummy(ImVec2(space_left_normal - 18, 0));
	ImGui::SameLine();
	ImGui::Text("1.4 Hz");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(44, 0));
	ImGui::SameLine();
	ImGui::Text("Frequenz");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(61, 0));
	ImGui::SameLine();
	ImGui::Text("10 Hz");
	//wavelength text
	ImGui::Dummy(ImVec2(space_left_normal - 18, 0));
	ImGui::SameLine();
	ImGui::Text("10 cm");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(35, 0));
	ImGui::SameLine();
	ImGui::Text(u8"Wellenl\u00E4nge");
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(22, 0));
	ImGui::SameLine();
	ImGui::Text("1.4 cm");

	ImGui::End();
}

void GuiHandler::draw_old_gui_(Toolbox& tb){
	/*Set Window Position and Size and start the Window*/
	ImGui::Begin("Controller", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);

	/*Block Management*/
	ImGui::Text("Mode Selection");
	ImGui::Text(" ");
	ImGui::Text("Blocks");
	ImGui::RadioButton("Immediate", &tb.m_state, static_cast<int>(MSTATE::IMMEDIATE));
	ImGui::SameLine();
	ImGui::RadioButton("Place", &tb.m_state, static_cast<int>(MSTATE::PLACE));
	ImGui::SameLine();
	ImGui::RadioButton("Delete", &tb.m_state, static_cast<int>(MSTATE::DELETE));
	ImGui::SameLine();
	ImGui::RadioButton("Move", &tb.m_state, static_cast<int>(MSTATE::MOVE));
	ImGui::SameLine();
	if (ImGui::Button("Clear Blocks")) {
		/*Send Clearing Message to Blockchain*/
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
	}
	if (tb.m_state == static_cast<int>(MSTATE::PLACE) or tb.m_state == static_cast<int>(MSTATE::IMMEDIATE)) {
		ImGui::SliderInt("Block Width", &tb.block_width, 10, 100);
		ImGui::SliderInt("Block Height", &tb.block_height, 10, 100);
	}
	ImGui::Text(" ");

	/*Drawing Management*/
	ImGui::Text("Free Draw");
	ImGui::RadioButton("Draw", &tb.m_state, static_cast<int>(MSTATE::DRAW));
	ImGui::SameLine();
	ImGui::RadioButton("Erase", &tb.m_state, static_cast<int>(MSTATE::ERASE));
	ImGui::SameLine();
	if (ImGui::Button("Clear Drawing")) {
		/*Send Clearing Message to Drawer*/
		tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR }); /*TODO: Drawer decides if it needs to message Blockchain & Wave*/
	}
	ImGui::Text(" ");

	/*Prebuilt Structures Management*/
	ImGui::Text("Prebuilt Structures");
	if (ImGui::Button("Single Slit")) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_SINGLESLIT });
	}
	if (ImGui::Button("Double Slit")) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_DOUBLESLIT });
	}
	if (ImGui::Button("Lattice")) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_LATTICE });
	}
	if (ImGui::Button("Waveguide")) {
		tb.mailbox.push_back({ BLOCKCHAINMESSAGE::PATTERN_WAVEGUIDE });
	}
	ImGui::Text(" ");

	/*Dynamics Management*/
	ImGui::Text("Dynamics Selection");
	ImGui::RadioButton("Run", &tb.s_state, static_cast<int>(SSTATE::RUN));
	ImGui::SameLine();
	ImGui::RadioButton("Stop", &tb.s_state, static_cast<int>(SSTATE::STOP));
	ImGui::SliderFloat("Source Amplitude", &tb.source_amplitude, 0.01f, 0.5f);
	ImGui::SliderFloat("Source Frequency", &tb.source_frequency, 1.f, 15.f);
	if (ImGui::Button("Reset Wave")) {
		/*TODO: The Parameter Reset should probably be done by the WaveHandler
			Delete these lines once it's done*/
			//tb.source_amplitude = 0.3;
			//tb.source_frequency = 10.;
			tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
		}
		ImGui::SameLine();
		if (ImGui::Button("Reset All")) {
			tb.mailbox.push_back({ WAVEMESSAGE::RESET_WAVE });
			tb.mailbox.push_back({ WAVEMESSAGE::RESET_DAMPING });
			tb.mailbox.push_back({ BLOCKCHAINMESSAGE::CLEAR });
			tb.mailbox.push_back({ DRAWERMESSAGE::CLEAR });
		}
		ImGui::End();
}

void GuiHandler::load_button_textures_(const std::string path) {
	/*Immediate Mode Textures*/
	std::string file = path + "immediate_off.png";
	load_image_to_texture_(file, btex_immediate_off_);
	file = path + "immediate_on.png";
	load_image_to_texture_(file, btex_immediate_on_);
	/*Draw Mode Textures*/
	file = path + "draw_off.png";
	load_image_to_texture_(file, btex_draw_off_);
	file = path + "draw_on.png";
	load_image_to_texture_(file, btex_draw_on_);
	/*Erase Mode Textures*/
	file = path + "erase_off.png";
	load_image_to_texture_(file, btex_erase_off_);
	file = path + "erase_on.png";
	load_image_to_texture_(file, btex_erase_on_);
	/*Singleslit Texture*/
	file = path + "singleslit_off.png";
	load_image_to_texture_(file, btex_singleslit_off_);
	/*Doubleslit Texture*/
	file = path + "doubleslit_off.png";
	load_image_to_texture_(file, btex_doubleslit_off_);
	/*Lattice Texture*/
	file = path + "lattice_off.png";
	load_image_to_texture_(file, btex_lattice_off_);
	/*Waveguide Texture*/
	file = path + "waveguide_off.png";
	load_image_to_texture_(file, btex_waveguide_off_);
	/*SSH Texture*/
	file = path + "ssh_off.png";
	load_image_to_texture_(file, btex_ssh_off_);
	/*Fresnel Texture*/
	file = path + "fresnel_off.png";
	load_image_to_texture_(file, btex_fresnel_off_);
}

void GuiHandler::load_image_to_texture_(const std::string file, GLuint& texture) {
	int width, height, nrChannels;
	unsigned char* data;
	data = stbi_load(file.c_str(), &width, &height, &nrChannels, STBI_rgb_alpha);
	if (!data) {
		std::cout << "Failed to load file " << file << std::endl;
		return;
	}
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
}