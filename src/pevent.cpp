#include <pevent.hpp>
#include <SDL.h>
#include <iostream>

Pevent PeventFactory::operator()(const SDL_Event& e) const
{
	float event_x = -1.f, event_y = -1.f; //negative numbers signify an issue (or SDL_QUIT)
	bool touch_event = false; //Is it a touch event?
	switch (e.type) {
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		event_x = static_cast<float>(e.button.x);
		event_y = static_cast<float>(e.button.y);
		break;
	case SDL_MOUSEMOTION:
		event_x = static_cast<float>(e.motion.x);
		event_y = static_cast<float>(e.motion.y);
		break;
	case SDL_FINGERDOWN:
	case SDL_FINGERUP:
	case SDL_FINGERMOTION:
		touch_event = true;
		event_x = static_cast<float>(e.tfinger.x);
		event_y = static_cast<float>(e.tfinger.y);
		break;
	case SDL_QUIT:
		break; //Nothing to see here
	default:
		std::cout << "Invalid event type in PeventFactory: " << e.type << std::endl;
		break;
	}
	Pevent pevent(e);
	pevent.fscoord_x = event_x;
	pevent.fscoord_y = event_y;
	if (!touch_event) {
		pevent.fscoord_x /= static_cast<float>(screen_w_);
		pevent.fscoord_y /= static_cast<float>(screen_h_);
	}
	pevent.itcoord_x = pevent.fscoord_x * (texture_w_ - texoffset_left_ - texoffset_right_) + texoffset_left_;
	pevent.itcoord_y = pevent.fscoord_y * (texture_h_ - texoffset_bottom_ - texoffset_top_) + texoffset_bottom_;
	
	return pevent;
}