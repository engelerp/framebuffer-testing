#include <glad/glad.h>
//#include <SDL2/SDL.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <init.hpp>
#include <infrastructure.hpp>
#include <shader.hpp>
#include <chrono>
#include <vector>
#include <timer.hpp>

#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>

#include <list>
#include "..\include\drawer.hpp"

//A: 0.4096
//B: 0.3616
//dt: 0.016

#define WIDTH_LOCAL 2048
#define HEIGHT_LOCAL 1024
#define WIDTH_4K 3840
#define HEIGHT_4K 2160

#define WIDTH WIDTH_LOCAL
#define HEIGHT HEIGHT_LOCAL
#define SCALE 2
#define TEXWIDTH WIDTH_LOCAL
#define TEXHEIGHT HEIGHT_LOCAL
#define UPDATES_PER_FRAME 2
#define FRAME_THRESHOLD 14.6f

//VRAM profiling on nVidia
#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049


//we could work with other channels to make the eraser round. communicate with alpha whether to incorporate the change or not. maybe we even need a different shader for that.
class Drawer{
public:
  Drawer(unsigned int texture, std::string shaderPath, int width=25, float reset_value=0.999f)
  : texture_(texture), width_(width), reset_value_(reset_value), drawing_(false), points_(32,0.f), shader_draw_((shaderPath+"draw.vert").c_str(), (shaderPath+"draw.frag").c_str()), num_drawn_(0)
  {
    //initialize drawing
    //VAO, VBO
    glGenVertexArrays(1, &VAO_draw_);
    glGenBuffers(1, &VBO_draw_);
    glBindVertexArray(VAO_draw_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_draw_);
    glBufferData(GL_ARRAY_BUFFER, points_.size()*sizeof(float), &points_[0], GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    //FBO
    glGenFramebuffers(1, &FBO_draw_);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO_draw_);
    //attach texture to framebuffer
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_, 0);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  	 std::cout << "ERROR::FRAMEBUFFER:: Drawing framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


  }

  void draw(int xoffset, int yoffset){
    ++num_drawn_;
    x0_ = x1_;
    y0_ = y1_;
    x1_ = 2.f*(float)xoffset/(float)TEXWIDTH - 1.f;
    y1_ = 2.f*(float)yoffset/(float)TEXHEIGHT - 1.f;
    //update the points
    if(!calculate_points_())
      return;
    //upload vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO_draw_);
    glBufferData(GL_ARRAY_BUFFER, points_.size()*sizeof(float), &points_[0], GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    glBindFramebuffer(GL_FRAMEBUFFER, FBO_draw_);
    glViewport(0,0,TEXWIDTH,TEXHEIGHT);
    shader_draw_.use();
    shader_draw_.setFloat("new_value", 0.0f);
    glBindVertexArray(VAO_draw_);
    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glDrawArrays(GL_TRIANGLE_FAN, 0, 16);
    //glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
    glBindVertexArray(0);
  }
  void erase(int xoffset, int yoffset){
    ++num_drawn_;
    x0_ = x1_;
    y0_ = y1_;
    x1_ = 2.f*(float)xoffset/(float)TEXWIDTH - 1.f;
    y1_ = 2.f*(float)yoffset/(float)TEXHEIGHT - 1.f;
    //update the points
    int width_stack = width_;
    width_ *= 2.5;
    if(!calculate_points_()){
      width_ = width_stack;
      return;
    }
    width_ = width_stack;
    //upload vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO_draw_);
    glBufferData(GL_ARRAY_BUFFER, points_.size()*sizeof(float), &points_[0], GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    glBindFramebuffer(GL_FRAMEBUFFER, FBO_draw_);
    glViewport(0,0,TEXWIDTH,TEXHEIGHT);
    shader_draw_.use();
    shader_draw_.setFloat("new_value", reset_value_);
    glBindVertexArray(VAO_draw_);
    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glDrawArrays(GL_TRIANGLE_FAN, 0, 16);
    //glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
    glBindVertexArray(0);
  }
  bool drawing(){ return drawing_; }
  void start_drawing(int x, int y){
    drawing_ = true;
    x0_ = 2.f*(float)x/(float)TEXWIDTH - 1.f;
    y0_ = 2.f*(float)y/(float)TEXHEIGHT - 1.f;
    x1_ = 2.f*(float)x/(float)TEXWIDTH - 1.f;
    y1_ = 2.f*(float)y/(float)TEXHEIGHT - 1.f;
  }
  void stop_drawing() { drawing_ = false; }
  int width(){ return width_; }
  int num_drawn() { return num_drawn_; }

private:
  bool calculate_points_(){
    glm::vec2 r_unnorm (x1_-x0_, y1_-y0_);
    if(glm::length(r_unnorm) == 0)
      return false;
    float target_length = (float)(width_) / (float)(TEXWIDTH);
    glm::vec2 r = target_length/(2.f*glm::length(r_unnorm))*r_unnorm;
    glm::vec2 r_L = glm::vec2(-r.y,r.x);
    glm::vec2 r_R = glm::vec2(r.y,-r.x);
    float cosinus = 0.866025f;
    float sinus = 0.5f;
    glm::vec2 v1 (cosinus*r_L.x - sinus*r_L.y, +sinus*r_L.x + cosinus*r_L.y);
    glm::vec2 v2 (cosinus*v1.x - sinus*v1.y, +sinus*v1.x + cosinus*v1.y);
    glm::vec2 v3 (cosinus*v2.x - sinus*v2.y, +sinus*v2.x + cosinus*v2.y);
    glm::vec2 v4 (cosinus*v3.x - sinus*v3.y, +sinus*v3.x + cosinus*v3.y);
    glm::vec2 v5 (cosinus*v4.x - sinus*v4.y, +sinus*v4.x + cosinus*v4.y);
    points_[0] = (x0_+x1_)/2.f;
    points_[1] = (y0_+y1_)/2.f;
    points_[2] = x0_ + r_L.x;
    points_[3] = y0_ + r_L.y;
    points_[4] = x0_ + v1.x;
    points_[5] = y0_ + v1.y;
    points_[6] = x0_ + v2.x;
    points_[7] = y0_ + v2.y;
    points_[8] = x0_ + v3.x;
    points_[9] = y0_ + v3.y;
    points_[10] = x0_ + v4.x;
    points_[11] = y0_ + v4.y;
    points_[12] = x0_ + v5.x;
    points_[13] = y0_ + v5.y;
    points_[14] = x0_ + r_R.x;
    points_[15] = y0_ + r_R.y;
    points_[16] = x1_ + r_R.x;
    points_[17] = y1_ + r_R.y;
    points_[18] = x1_ - v1.x;
    points_[19] = y1_ - v1.y;
    points_[20] = x1_ - v2.x;
    points_[21] = y1_ - v2.y;
    points_[22] = x1_ - v3.x;
    points_[23] = y1_ - v3.y;
    points_[24] = x1_ - v4.x;
    points_[25] = y1_ - v4.y;
    points_[26] = x1_ - v5.x;
    points_[27] = y1_ - v5.y;
    points_[28] = x1_ + r_L.x;
    points_[29] = y1_ + r_L.y;
    points_[30] = x0_ + r_L.x;
    points_[31] = y0_ + r_L.y;
    return true;
  }
  unsigned int texture_;
  unsigned int VAO_draw_, VBO_draw_;
  unsigned int FBO_draw_;
  int width_;
  float reset_value_;
  bool drawing_;
  float x0_, y0_;
  float x1_, y1_;
  std::vector<float> points_;
  Shader shader_draw_;
  int num_drawn_;

};


//to be continued
class Block{
public:
  Block(): needs_upload_(false){}
  Block(const Block& o)
  : width_(o.width_), height_(o.height_), xoffset_(o.xoffset_), yoffset_(o.yoffset_), texture_(o.texture_), data_(o.data_), needs_upload_(o.needs_upload_), needs_delete_(o.needs_delete_), reset_value_(o.reset_value_)
  {}
  Block(int width, int height, int xoffset, int yoffset, unsigned int texture, float new_value = 0.0, float reset_value = 0.999)
  : width_(width), height_(height), xoffset_(xoffset), yoffset_(yoffset), texture_(texture), data_(4*width*height,new_value), needs_upload_(true), needs_delete_(false), reset_value_(reset_value)
  {}
  bool needs_upload() { return needs_upload_; }
  bool needs_delete() { return needs_delete_; }
  void request_delete() { needs_delete_ = true; }
  void update_upload(){
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset_, yoffset_, width_, height_, GL_RGBA, GL_FLOAT, data_.data());
    glBindTexture(GL_TEXTURE_2D, 0);
    needs_upload_ = false;
  }
  void update_delete(){
    data_ = std::vector<float>(4*width_*height_,reset_value_);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset_, yoffset_, width_, height_, GL_RGBA, GL_FLOAT, data_.data());
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  bool is_inside(int x, int y){
    return ((x > xoffset_) && (x < xoffset_+width_) && (y > yoffset_) && (y < yoffset_+height_));
  }
  Block translated(int x, int y){
    return Block(width_, height_, x, y, texture_, 0.0, reset_value_);
  }
  int xoffset() {return xoffset_;}
  int yoffset() {return yoffset_;}
private:
  std::vector<float> data_;
  int width_, height_, xoffset_, yoffset_;
  bool needs_upload_;
  bool needs_delete_;
  float reset_value_;
  unsigned int texture_;
};

void upload_blocks(std::list<Block>* blockchain){
  for(auto it = blockchain->begin(); it != blockchain->end(); ++it){
    if(it->needs_upload())
      it->update_upload();
  }
}

bool delete_blocks(std::list<Block>* blockchain){
  bool have_deleted = false;
  auto it = blockchain->begin();
  while(it != blockchain->end()){
    if(it->needs_delete()){
      have_deleted = true;
      it->update_delete();
      blockchain->erase(it++);
    }
    else
      ++it;
  }
  return have_deleted;
}

void clear_blocks(std::list<Block>* blockchain){
  auto it = blockchain->begin();
  while(it != blockchain->end()){
    it->update_delete();
    blockchain->erase(it++);
  }
}

void reload_blocks(std::list<Block>* blockchain){
  for(auto it = blockchain->begin(); it != blockchain->end(); ++it){
    it->update_upload();
  }
}

void mark_delete(std::list<Block>* blockchain, int x, int y){
  for(auto it = blockchain->begin(); it != blockchain->end(); ++it){
    std::cout << it->is_inside(x,y) << std::endl;
    if(it->is_inside(x,y)){
      it->request_delete();
      return;
    }

  }
}

std::list<Block> create_single_slit(unsigned int texture){
  std::list<Block> blocks;

  int dim = 30;
  int xCoord = (int)(0.5*TEXWIDTH);
  int yStart = (int)(0.2*TEXHEIGHT);
  for(int n = 0; n < 0.6*TEXHEIGHT/30.; ++n){
    blocks.push_back(Block(dim, dim, xCoord, yStart + n*dim, texture));
  }
  auto it = blocks.begin();
  int inc = 0;
  while(inc < blocks.size()/2){
    ++it; ++inc;
  }
  blocks.erase(it);

  return blocks;
}
std::list<Block> create_double_slit(unsigned int texture){
  std::list<Block> blocks;

  int dim = 30;
  int xCoord = (int)(0.5*TEXWIDTH);
  int yStart = (int)(0.2*TEXHEIGHT);
  for(int n = 0; n < 0.6*TEXHEIGHT/30.; ++n){
    blocks.push_back(Block(dim, dim, xCoord, yStart + n*dim, texture));
  }
  auto it = blocks.begin();
  int inc = 0;
  while(inc < blocks.size()/2-1){
    ++it; ++inc;
  }
  blocks.erase(it++);
  blocks.erase(++it);

  return blocks;
}

std::list<Block> create_lattice(unsigned int texture){
  std::list<Block> blocks;

  int width = 11;
  int height = 10;
  int yStart = (int)(0.2*TEXHEIGHT+0.5*height);
  int xCoord = (int)(0.35*TEXWIDTH); //0.35
  for(int i = 0; i < 28; ++i){ //columns 28
    for(int n = 0; n < 0.6*TEXHEIGHT; n+=2*height){
      blocks.push_back(Block(width,height,xCoord,yStart+n,texture));
    }
    xCoord += 2*width;
  }
  return blocks;
}

std::list<Block> create_waveguide(unsigned int texture){
  std::list<Block> blocks;

  int width = 11;
  int height = 10;
  int yStart = (int)(0.2*TEXHEIGHT+0.5*height);
  int xCoord = (int)(0.35*TEXWIDTH);
  for(int i = 0; i < 28; ++i){ //columns
    for(int n = 0; n < 0.6*TEXHEIGHT; n+=2*height){
      if(yStart+n > 0.57*TEXHEIGHT || yStart+n < 0.42*TEXHEIGHT)
        blocks.push_back(Block(width,height,xCoord,yStart+n,texture));
    }
    xCoord += 2*width;
  }
  return blocks;
}



int main(int argc, char** argv){
  Infrastructure infra (Init("Wave Window", WIDTH, HEIGHT));

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;
  ImGui::StyleColorsDark();
  ImGui_ImplSDL2_InitForOpenGL(infra.window(), infra.context());
  const char* glsl_version = "#version 330";
  ImGui_ImplOpenGL3_Init(glsl_version);

  int w_width, w_height;
  SDL_GetWindowSize(infra.window(), &w_width, &w_height);

  //-----------------------
  //Data generation
  //-----------------------
  std::cout << "Filling Data" << std::endl;
  unsigned int tot_height = TEXHEIGHT;
  unsigned int tot_width = TEXWIDTH;
  std::vector<float> texData (4*tot_height*tot_width, 0.f);
  for(int i = 0; i < texData.size()/4; ++i){ //alpha
    texData[4*i+3] = 1.0f;
  }
  for(int i = 0; i < 0.6*TEXHEIGHT; ++i){ //source
    texData[static_cast<int>(0.2*TEXHEIGHT)*TEXWIDTH*4 + 4*i*TEXWIDTH + 4*409+2 -200] = 1.0f; //-200: move source into damped region by 50px, spurious reflections behave better
  }

  std::cout << "Data Filled" << std::endl;
  std::cout << "Float size: " << sizeof(float) << std::endl;
  std::cout << "Width: " << WIDTH << std::endl;
  std::cout << "Height: " << HEIGHT << std::endl;
  std::cout << "Texture Width: " << TEXWIDTH << std::endl;
  std::cout << "Texture Height: " << TEXHEIGHT << std::endl;

  std::cout << "Data size: " << texData.size() << std::endl;
  //-----------------------
  //IDEA:
  //Framebuffer 1: t-1
  //Framebuffer 2: t
  //Framebuffer 3: t+1
  //
  //(1): use shader step.vert/step.frag to step t -> t+1
  //(2): use shader copy.vert/copy.frag twice to copy t -> t-1 and t+1 -> t
  //(3): render texture t+1 to screen
  //-----------------------


  //-----------------------
  //Framebuffer 1 setup
  //-----------------------
  unsigned int framebuffer1;
  glGenFramebuffers(1, &framebuffer1);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer1);

  //texture for attachment
  unsigned int fb_texture1;
  glGenTextures(1, &fb_texture1);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, fb_texture1);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  //attach texture to framebuffer
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fb_texture1, 0);

  //check framebuffer
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	 std::cout << "ERROR::FRAMEBUFFER:: Framebuffer 1 is not complete!" << std::endl;

  //unbind framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, 0);





  //-----------------------
  //Framebuffer 2 setup
  //-----------------------
  unsigned int framebuffer2;
  glGenFramebuffers(1, &framebuffer2);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer2);

  //texture for attachment
  unsigned int fb_texture2;
  glGenTextures(1, &fb_texture2);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, fb_texture2);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  //attach texture to framebuffer
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fb_texture2, 0);

  //check framebuffer
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	 std::cout << "ERROR::FRAMEBUFFER:: Framebuffer 2 is not complete!" << std::endl;

  //unbind framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, 0);











  //-----------------------
  //VAO setup
  //-----------------------
  //texture attributes
  std::vector<float> pos_uv_tex = {//x      y       z       U       V
                                  1.f,    1.f,    0.f,    1.f,    1.f,
                                  1.f,    -1.f,   0.f,    1.f,    0.f,
                                  -1.f,   -1.f,   0.f,    0.f,    0.f,
                                  -1.f,   -1.f,   0.f,    0.f,    0.f,
                                  1.f,    1.f,    0.f,    1.f,    1.f,
                                  -1.f,   1.f,    0.f,    0.f,    1.f};

  unsigned int VBO_tex, VAO_tex;
  glGenVertexArrays(1, &VAO_tex);
  glGenBuffers(1, &VBO_tex);
  glBindVertexArray(VAO_tex);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_tex);
  glBufferData(GL_ARRAY_BUFFER, pos_uv_tex.size()*sizeof(float), &pos_uv_tex[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);

  //screen attributes
  float off_screen = 0.2; //UV-portion that is placed outside of the screen at each boundary
  std::vector<float> pos_uv_screen = {//x      y       z       U       V
                                      1.f,    1.f,    0.f,    1.f-off_screen,    1.f-off_screen,
                                      1.f,    -1.f,   0.f,    1.f-off_screen,    0.f+off_screen,
                                      -1.f,   -1.f,   0.f,    0.f+off_screen,    0.f+off_screen,
                                      -1.f,   -1.f,   0.f,    0.f+off_screen,    0.f+off_screen,
                                      1.f,    1.f,    0.f,    1.f-off_screen,    1.f-off_screen,
                                      -1.f,   1.f,    0.f,    0.f+off_screen,    1.f-off_screen};

  unsigned int VBO_screen, VAO_screen;
  glGenVertexArrays(1, &VAO_screen);
  glGenBuffers(1, &VBO_screen);
  glBindVertexArray(VAO_screen);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_screen);
  glBufferData(GL_ARRAY_BUFFER, pos_uv_screen.size()*sizeof(float), &pos_uv_screen[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);





  //-----------------------
  //Damping texture setup
  //-----------------------
    //can potentially go to GL_R32F
  std::vector<float> texDamping (4*tot_height*tot_width, 0.992f);
  int offset = static_cast<int>(off_screen*TEXHEIGHT)*TEXWIDTH*4 + static_cast<int>(off_screen*TEXWIDTH)*4;
  for(int i = 0; i < (1.f-2.f*off_screen)*TEXHEIGHT; ++i){
    for(int j = 0; j < (1.f-2.f*off_screen)*TEXWIDTH; ++j){
      texDamping[offset + 4*TEXWIDTH*i + 4*j] = 0.999f;
      texDamping[offset + 4*TEXWIDTH*i + 4*j+1] = 0.0f;
      texDamping[offset + 4*TEXWIDTH*i + 4*j+2] = 0.0f;
      texDamping[offset + 4*TEXWIDTH*i + 4*j+3] = 0.0f;
    }
  }

  unsigned int damping_texture;
  glGenTextures(1, &damping_texture);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, damping_texture);

  //can potentially go to GL_R32F
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texDamping.data());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);










  //-----------------------
  //Shader setup
  //-----------------------
  //std::string shaderPath = "/Users/pascal/framebuffer_testing/shaders/"; //MACOS
  std::string shaderPath = "C:\\Users\\engel\\VS_Projects\\FocusTerra\\framebuffer-testing\\shaders\\"; //WINDOWS

  if(argc == 2){
    std::cout << "Shader Path: " << argv[1] << std::endl;
    shaderPath = argv[1];
  }

  Shader shader_step ((shaderPath+"step.vert").c_str(), (shaderPath+"step.frag").c_str());
  Shader shader_copy ((shaderPath+"copy.vert").c_str(), (shaderPath+"copy.frag").c_str());
  Shader shader_copyred ((shaderPath+"copy_red.vert").c_str(), (shaderPath+"copy_red.frag").c_str());

  //set shader uniforms
  shader_step.use();
  shader_step.setFloat("dx", 1./TEXWIDTH);
  shader_step.setFloat("dy", 1./TEXHEIGHT);
  shader_step.setFloat("c1", 0.4096);
  shader_step.setFloat("c2", 0.3616);
  shader_step.setInt("texture1",0);
  shader_step.setInt("texture2",1);

  shader_copy.use();
  shader_copy.setInt("texture1",0);

  shader_copyred.use();
  shader_copyred.setInt("texture1",0);
  shader_copyred.setInt("texture2",1);





  std::cout << "Setup Complete" << std::endl;








  //-----------------------
  //Game States
  //-----------------------
  enum class MSTATE {PLACE, DELETE, MOVE, DRAW, ERASE};
  enum class GSTATE {RUN, STOP};

  MSTATE m_state = MSTATE::PLACE;
  GSTATE g_state = GSTATE::RUN;

  bool dragging = false;
  int drag_x_offset = 0;
  int drag_y_offset = 0;


  //debug parameters
  int VAO_selection = 0;
  int block_width = 30;
  int block_height = 30;





  //-----------------------
  //Block List
  //-----------------------
  std::list<Block> blockchain;
  int margin_offset_x = (int)(off_screen*TEXWIDTH);
  int margin_offset_y = (int)(off_screen*TEXHEIGHT);
  blockchain.push_back(Block(30,30,margin_offset_x+500, margin_offset_y+200, damping_texture));






  //-----------------------
  //Drawer
  //-----------------------
  Drawer drawer (damping_texture, shaderPath);








  //-----------------------
  //Game Loop
  //-----------------------

  bool is_running = true;
  SDL_Event event;
  Timer timer;
  float time = 0.f;
  int count = 0;
  bool need_reload = false;
  float sourceAmplitude = 0.3;
  float sourceFrequency = 10.;
  //game loop
  while (is_running) {
    ++count;
    timer.start();












    //-----------------------
    //Event Handling
    //-----------------------
    while (SDL_PollEvent(&event)) {
      //Pass event to imgui
      ImGui_ImplSDL2_ProcessEvent(&event);
      //now do our own processing
      if (event.type == SDL_QUIT) {
        is_running = false;
      }

      //different GUI states
      if(m_state == MSTATE::PLACE){
        if(event.type == SDL_MOUSEBUTTONDOWN && !io.WantCaptureMouse){
          if(VAO_selection == 0){ //normal view
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            blockchain.push_back(Block(block_width, block_height, real_screen_mouse_x - block_width/2, real_screen_mouse_y-block_height/2, damping_texture));
          }
          else{ //debug view
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            blockchain.push_back(Block(block_width, block_height, real_tex_mouse_x - block_width/2, real_tex_mouse_y-block_height/2, damping_texture));
          }
        }
      }
      if(m_state == MSTATE::DELETE){
        if(event.type == SDL_MOUSEBUTTONDOWN && !io.WantCaptureMouse){
          if(VAO_selection == 0){ //normal view
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            mark_delete(&blockchain, real_screen_mouse_x, real_screen_mouse_y);
          }
          else{ //debug view
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            mark_delete(&blockchain, real_tex_mouse_x, real_tex_mouse_y);
          }
        }
      }
      if(m_state == MSTATE::MOVE){
        if(event.type == SDL_MOUSEBUTTONDOWN && !io.WantCaptureMouse){
          if(VAO_selection == 0){ //normal view
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            for(auto it = blockchain.begin(); it != blockchain.end(); ++it){
              if(it->is_inside(real_screen_mouse_x, real_screen_mouse_y)){ //push dragged rectangle to the back of the blockchain
                Block backup (*it);
                blockchain.erase(it);
                blockchain.push_back(backup);
                dragging = true;
                drag_x_offset = blockchain.back().xoffset() - real_screen_mouse_x;
                drag_y_offset = blockchain.back().yoffset() - real_screen_mouse_y;
                break;
              }
            }
          }
          else{ //debug view
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            for(auto it = blockchain.begin(); it != blockchain.end(); ++it){
              if(it->is_inside(real_tex_mouse_x, real_tex_mouse_y)){ //push dragged rectangle to the back of the blockchain
                Block backup (*it);
                blockchain.erase(it);
                blockchain.push_back(backup);
                dragging = true;
                drag_x_offset = blockchain.back().xoffset() - real_tex_mouse_x;
                drag_y_offset = blockchain.back().yoffset() - real_tex_mouse_y;
                break;
              }
            }
          }
        }
        if(event.type == SDL_MOUSEMOTION && dragging){
          if(VAO_selection == 0){
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            Block shiftedBlock = blockchain.back().translated(real_screen_mouse_x + drag_x_offset, real_screen_mouse_y + drag_y_offset);
            blockchain.back().update_delete();
            auto iterator = blockchain.end();
            blockchain.erase(--iterator);
            blockchain.push_back(shiftedBlock);
            blockchain.back().update_upload();
            need_reload = true;
          }
          else{
            int real_screen_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_screen_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            Block shiftedBlock = blockchain.back().translated(real_screen_mouse_x + drag_x_offset, real_screen_mouse_y + drag_y_offset);
            blockchain.back().update_delete();
            auto iterator = blockchain.end();
            blockchain.erase(--iterator);
            blockchain.push_back(shiftedBlock);
            blockchain.back().update_upload();
            need_reload = true;
          }
        }
        if(event.type == SDL_MOUSEBUTTONUP){
          dragging = false;
        }
      }
      if(m_state == MSTATE::DRAW){
        if(event.type == SDL_MOUSEBUTTONDOWN && !io.WantCaptureMouse){
          if(VAO_selection == 0){ //normal view
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            drawer.start_drawing(real_screen_mouse_x, real_screen_mouse_y);
            drawer.draw(real_screen_mouse_x, real_screen_mouse_y);
          }
          else{ //debug view
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            drawer.start_drawing(real_tex_mouse_x, real_tex_mouse_y);
            drawer.draw(real_tex_mouse_x, real_tex_mouse_y);
          }
        }
        if(event.type == SDL_MOUSEMOTION && drawer.drawing()){
          if(VAO_selection == 0){
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            drawer.draw(real_screen_mouse_x, real_screen_mouse_y);
          }
          else{
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            drawer.draw(real_tex_mouse_x, real_tex_mouse_y);
          }
        }
        if(event.type == SDL_MOUSEBUTTONUP){
          drawer.stop_drawing();
        }
      }
      if(m_state == MSTATE::ERASE){
        if(event.type == SDL_MOUSEBUTTONDOWN && !io.WantCaptureMouse){
          if(VAO_selection == 0){ //normal view
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            drawer.start_drawing(real_screen_mouse_x, real_screen_mouse_y);
            drawer.erase(real_screen_mouse_x, real_screen_mouse_y);
            need_reload = true;
          }
          else{ //debug view
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            drawer.start_drawing(real_tex_mouse_x, real_tex_mouse_y);
            drawer.erase(real_tex_mouse_x, real_tex_mouse_y);
            need_reload = true;
          }
        }
        if(event.type == SDL_MOUSEMOTION && drawer.drawing()){
          if(VAO_selection == 0){
            int real_screen_mouse_x = margin_offset_x + (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * 0.6 * TEXWIDTH);
            int real_screen_mouse_y = margin_offset_y + (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * 0.6 * TEXHEIGHT);
            drawer.erase(real_screen_mouse_x, real_screen_mouse_y);
            need_reload = true;
          }
          else{
            int real_tex_mouse_x = (int)((float)(io.MousePos.x) / (float)(io.DisplaySize.x) * TEXWIDTH);
            int real_tex_mouse_y = (int)((1.f - (float)(io.MousePos.y) / (float)(io.DisplaySize.y)) * TEXHEIGHT);
            drawer.erase(real_tex_mouse_x, real_tex_mouse_y);
            need_reload = true;
          }
        }
        if(event.type == SDL_MOUSEBUTTONUP){
          drawer.stop_drawing();
        }
      }
    }









    //ImGui handling
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(infra.window());
    ImGui::NewFrame();

    ImGui::Begin("Controller", NULL, ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::Text("Mode Selection");
    ImGui::Text(" ");
    ImGui::Text("Blocks");
    ImGui::RadioButton("Place", (int*)&m_state, (int)MSTATE::PLACE);
    ImGui::SameLine();
    ImGui::RadioButton("Delete", (int*)&m_state, (int)MSTATE::DELETE);
    ImGui::SameLine();
    ImGui::RadioButton("Move", (int*)&m_state, (int)MSTATE::MOVE);
    ImGui::SameLine();
    if(ImGui::Button("Clear Blocks")){
      clear_blocks(&blockchain);
    }
    if(m_state == MSTATE::PLACE){
      ImGui::SliderInt("Block Width", &block_width, 10, 50);
      ImGui::SliderInt("Block Height", &block_height, 10, 50);
    }
    ImGui::Text(" ");
    ImGui::Text("Free Draw");
    ImGui::RadioButton("Draw", (int*)&m_state, (int)MSTATE::DRAW);
    ImGui::SameLine();
    ImGui::RadioButton("Erase", (int*)&m_state, (int)MSTATE::ERASE);
    ImGui::SameLine();
    if(ImGui::Button("Clear Drawing")){
      //reset the damping mask
      glBindTexture(GL_TEXTURE_2D, damping_texture);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texDamping.data());
      //reload the blocks
      need_reload = true;
    }
    ImGui::Text(" ");
    ImGui::Text("Prebuilt Structures");
    if(ImGui::Button("Single Slit")){
      clear_blocks(&blockchain); 
      blockchain = create_single_slit(damping_texture);
    }
    ImGui::SameLine();
    if(ImGui::Button("Double Slit")){
      clear_blocks(&blockchain);
      blockchain = create_double_slit(damping_texture);
    }
    ImGui::SameLine();
    if(ImGui::Button("Lattice")){
      clear_blocks(&blockchain);
      blockchain = create_lattice(damping_texture);
    }
    ImGui::SameLine();
    if(ImGui::Button("Waveguide")){
      clear_blocks(&blockchain);
      blockchain = create_waveguide(damping_texture);
    }
    ImGui::Text(" ");
    ImGui::Text("Dynamics Selection");
    ImGui::RadioButton("Run", (int*)&g_state, (int)GSTATE::RUN);
    ImGui::SameLine();
    ImGui::RadioButton("Stop", (int*)&g_state, (int)GSTATE::STOP);
    ImGui::SliderFloat("Source Amplitude", &sourceAmplitude, 0.01, 0.5);
    ImGui::SliderFloat("Source Frequency", &sourceFrequency, 1., 15.);
    if(ImGui::Button("Reset Wave")){
      //reset the data textures
      glBindTexture(GL_TEXTURE_2D, fb_texture1);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());
      glBindTexture(GL_TEXTURE_2D, fb_texture2);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());

      time = 0.f;
      sourceAmplitude = 0.3;
      sourceFrequency = 10.;
    }
    ImGui::SameLine();
    if(ImGui::Button("Reset All")){
      //reset the data textures
      glBindTexture(GL_TEXTURE_2D, fb_texture1);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());
      glBindTexture(GL_TEXTURE_2D, fb_texture2);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texData.data());
      //reset the damping mask
      glBindTexture(GL_TEXTURE_2D, damping_texture);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_FLOAT, texDamping.data());
      //clear the blocks
      clear_blocks(&blockchain);
      //reset time
      time = 0.f;
      block_width = 30.;
      block_height = 30.;
      sourceAmplitude = 0.3;
      sourceFrequency = 10.;
    }
    ImGui::End();

    //Debug Window
    ImGui::SetNextWindowPos(ImVec2(10.f,10.f), ImGuiCond_Always, ImVec2(0.f,0.f));
    ImGui::SetNextWindowBgAlpha(0.35f);
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;
    ImGui::Begin("Developer", NULL, window_flags);
    ImGui::Text("Developer Window");
    ImGui::Text("Texture Resolution: %u x %u", TEXWIDTH, TEXHEIGHT);
    ImGui::Text("Window Resolution: %u x %u", (int)io.DisplaySize.x,(int)io.DisplaySize.y);
    ImGui::Text("Mouse Position: (%.1f, %.1f)", io.MousePos.x, io.MousePos.y);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    //VRAM GOES HERE
    GLint total_mem_kb=0, current_mem_kb=0;
    glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &total_mem_kb);
    glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &current_mem_kb);
    ImGui::Text("Total VRAM: %.3f MB", static_cast<float>(total_mem_kb)/1000.f);
    ImGui::Text("VRAM used: %.3f MB, VRAM free: %.3f MB", static_cast<float>(total_mem_kb - current_mem_kb)/1000.f, static_cast<float>(current_mem_kb)/1000.f);
    ImGui::Text("VAO selection");
    ImGui::SameLine();
    ImGui::RadioButton("Screen VAO", &VAO_selection, 0);
    ImGui::SameLine();
    ImGui::RadioButton("Texture VAO", &VAO_selection, 1);
    ImGui::Text("Blocks Present: %lu", blockchain.size());
    ImGui::Text("Line Segments Drawn: %u", drawer.num_drawn());
    ImGui::Text("Simulation Time: %f", time);
    ImGui::End();






    //update blockchain
    upload_blocks(&blockchain);
    bool need_reload_delete = delete_blocks(&blockchain);
    need_reload = need_reload | need_reload_delete;
    if(need_reload){
      reload_blocks(&blockchain);
      need_reload = false;
    }







    //-----------------------
    //Data update and rendering
    //-----------------------
    for(int updates = 0; updates < UPDATES_PER_FRAME; ++updates){

      //-----------------------
      //Rendering
      //-----------------------
      //Step 1
      if(g_state == GSTATE::STOP)
        time = 0.f;
      glBindFramebuffer(GL_FRAMEBUFFER, framebuffer1);
      //SDL_GetWindowSize(infra.window(), &w_width, &w_height);
      glViewport(0,0,TEXWIDTH,TEXHEIGHT);
      glClearColor(0.1f,0.1f,0.2f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      shader_step.use();
      shader_step.setFloat("t", time);
      shader_step.setFloat("amplitude", sourceAmplitude);
      shader_step.setFloat("frequency", sourceFrequency);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, fb_texture2);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, damping_texture);
      glBindVertexArray(VAO_tex);
      glDrawArrays(GL_TRIANGLES, 0, 6);
      glBindVertexArray(0);
      if(g_state == GSTATE::RUN)
        time += 0.016f;

      //Step 2
      glBindFramebuffer(GL_FRAMEBUFFER, framebuffer2);
      //SDL_GetWindowSize(infra.window(), &w_width, &w_height);
      glViewport(0,0,TEXWIDTH,TEXHEIGHT);
      glClearColor(0.1f,0.1f,0.2f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      shader_step.use();
      shader_step.setFloat("t", time);
      shader_step.setFloat("amplitude", sourceAmplitude);
      shader_step.setFloat("frequency", sourceFrequency);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, fb_texture1);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, damping_texture);
      glBindVertexArray(VAO_tex);
      glDrawArrays(GL_TRIANGLES, 0, 6);
      glBindVertexArray(0);
      if(g_state == GSTATE::RUN)
        time += 0.016f;
    }

    // glFlush();
    // while(timer.current() < FRAME_THRESHOLD){
    //   continue;
    // }

    //Step 3
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //fucking slow operation
    SDL_GetWindowSize(infra.window(), &w_width, &w_height);
    glViewport(0,0,w_width,w_height);
    glClearColor(0.1f,0.1f,0.2f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    //shader_copy.use();
    shader_copyred.use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, fb_texture2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, damping_texture);
    glBindVertexArray(VAO_screen);
    //glBindTexture(GL_TEXTURE_2D, damping_texture);
    if(VAO_selection == 1)
      glBindVertexArray(VAO_tex);
    glEnable(GL_MULTISAMPLE); //anti-aliasing
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);




    //-----------------------
    //Imgui Rendering
    //-----------------------
    ImGui::Render();
    glViewport(0,0,(int)io.DisplaySize.x,(int)io.DisplaySize.y);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData()); //THROWS IN WINDOWS



    //Swap buffers
    SDL_GL_SwapWindow(infra.window());

    if(count % 100 == 0){
      std::cout << 1000./timer.current() << " fps\n";
      std::cout << "Blockchain has " << blockchain.size() << " elements.\n";
    }
/*
    //Error checking
    std::cout << "Error Code: " << glGetError() << std::endl;
    std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
    std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
    std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
    std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
    std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
    std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
*/
  }


  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();
  infra.quit();

  return 0;
}
