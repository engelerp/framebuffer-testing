#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D texture1;

void main()
{
        vec4 color = texture(texture1, textureCoordinates);
        FragColor = vec4(0.25*(1.+color.r)+0.25, 0., 0., 1.);
}
