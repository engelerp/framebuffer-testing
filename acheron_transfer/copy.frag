#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;

uniform sampler2D texture1;

void main()
{
        FragColor = texture(texture1, textureCoordinates);
}
