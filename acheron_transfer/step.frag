#version 410 core

precision highp float;

out vec4 FragColor;

in vec2 textureCoordinates;
in vec2 downCoordinates;
in vec2 upCoordinates;
in vec2 leftCoordinates;
in vec2 rightCoordinates;
in float source;


uniform float dx;
uniform float dy;
uniform float c1;
uniform float c2;
uniform float t;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

//red is position, green is velocity

void main()
{

        vec4 returnColor = vec4(0.,0.,0.,0.);
        vec4 col = texture(texture2, textureCoordinates);
        float damp = texture(texture3, textureCoordinates).r;

        //source contribution
        float sourceColor = col.b*source;

        //contribution for non-sources
        float newvel = 0.;
        float newpos = 0.;
        float left = texture(texture2, leftCoordinates).r;
        float right = texture(texture2, rightCoordinates).r;
        float up = texture(texture2, upCoordinates).r;
        float down = texture(texture2, downCoordinates).r;
        float mid = 0.25*(left+right+up+down);
        newvel = (1.5*(mid-col.r) + col.g*damp)*(1.-col.b); //0.9999 instead of damp
        newpos = (col.r + newvel)*(1.-col.b);

        //mix source and non-source
        returnColor = vec4(newpos+sourceColor, newvel, col.b, col.a);

        FragColor = returnColor;
}
