#pragma once
#include <vector>
#include <glad/glad.h>
class Block {
public:
	Block() = default;
	Block(const Block&);
	Block(int w, int h, int xoffset, int yoffset, GLuint texture_damp, GLuint texture_wave);

	bool needs_upload() const;
	bool needs_removal() const;
	void request_removal();
	void upload();
	void remove();
	bool is_inside(int x, int y) const;
	//Block translated(int new_x, int new_y);
	void translate(int new_x, int new_y);
	int xoffset() const;
	int yoffset() const;
	int width() const;
	int height() const;
	GLuint& texture_damp();
	void switch_texture(GLuint);
	
private:
	int w_ = 0;
	int h_ = 0;
	int xoffset_ = 0;
	int yoffset_ = 0;
	bool needs_upload_ = true;
	bool needs_removal_ = false;
	GLuint texture_damp_ = 0;
	GLuint texture_wave_ = 0;
};