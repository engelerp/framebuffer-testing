#pragma once
#include <glad/glad.h>
#include <enums.hpp>
#include <vector>
#include <list>
#include <message.hpp>
#include <pevent.hpp>
#include <infrastructure.hpp>
#include <string>

struct Toolbox {
	Toolbox(const int screen_w, const int screen_h, //screen resolution
		const int texture_w, const int texture_h, //texture resolution
		const int texoffset_left, //texture offset left
		const int texoffset_right, //texture offset right
		const int texoffset_bottom, //texture offset bottom
		const int texoffset_top, //texture offset top
		const std::string shaderpath, //path to shaders
		const std::string texturepath, //path to textures
		const std::string resourcepath); //path to resources
	Toolbox(const Toolbox&) = delete;
	
	/*Clear Mailbox and Events*/
	void newFrame();
	
	
	
	/*Textures, these are all initialized by WaveHandler*/
	//GLuint tex_damp = 0; /*This one is now controlled by WaveHandler*/
	GLuint tex_damp_dynamic = 0; //Dynamic damping texture
	GLuint tex_damp_static = 0; //Static damping texture
	GLuint tex_wave_clean = 0; //Initial wave texture, including sources
	GLuint tex_damp_clean = 0; //Initial damping texture
	GLuint tex_const_zero = 0; //Texture with constant value 0 in all channels
	GLuint tex_wave_0 = 0, tex_wave_1 = 0; //The two computational wave textures
	/*general members*/
	std::vector<Message> mailbox;
	std::list<Pevent> events;
	PeventFactory pevFactory;

	/*members added for GuiHandler*/
	Infrastructure infra;
	int m_state; //mouse state
	int s_state; //source state
	int g_state; //game state
	int block_width; //placed Block width
	int block_height; //placed Block height
	float source_amplitude; //source amplitude
	float source_frequency; //source frequency
	int screen_w; //screen width in pixels
	int screen_h; //screen height in pixels
	int texture_w; //texture width in pixels
	int texture_h; //texture height in pixels

	/*members pertaining GuiHandler for others*/
	float gui_pos = 0.f; //initialized by GuiHandler::init

	/*members added for WaveHandler*/
	const int texoffset_left, texoffset_right;
	const int texoffset_bottom, texoffset_top;
	std::string shader_path;
	std::string texture_path;
	std::string resource_path;
	float time;
	float dt;

	/*members added for DrawingHandler*/
	int drawing_width = 50; //pencil width when drawing
	int erasing_width = 125; //eraser width when erasing

	/*developer window info*/
	size_t num_blocks = 0;
	size_t num_drawers = 0;
	int timeout_threshold = 0;
	int timeout_timer = 0;

	/*touch state info*/
	SDL_TouchID touch_device = 0;
	std::vector<SDL_FingerID> current_touchIDs;










};