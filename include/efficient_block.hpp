#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class EfficientBlock {
public:
	EfficientBlock() = default;
	EfficientBlock(const EfficientBlock&);
	EfficientBlock(int w, int h, int xoffset, int yoffset);

	void request_removal();
	void translate(int new_x, int new_y);

	bool needs_removal() const;
	glm::ivec4 xywh() const;
	bool is_inside(int x, int y) const;
	int width() const;
	int height() const;

private:
	glm::ivec4 xywh_;
	bool needs_removal_;
};