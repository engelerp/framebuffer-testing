#include <chrono>

class Timer{
public:
  Timer(){}

  void start(){ start_ = std::chrono::high_resolution_clock::now(); }
  void end(){ end_ = std::chrono::high_resolution_clock::now(); }
  float current(){
    std::chrono::time_point<std::chrono::high_resolution_clock> end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> diff = end-start_;
    return diff.count()*1000.;
  }
  float ms(){
    std::chrono::duration<float> diff = end_-start_;
    return diff.count()*1000.;
  }

private:
  std::chrono::time_point<std::chrono::high_resolution_clock> start_, end_;
};
