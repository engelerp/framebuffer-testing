#pragma once
#include <toolbox.hpp>
#include <list>
#include <SDL.h>
#include <drawer.hpp>
#include <shader.hpp>
#include <glad/glad.h>
#include <array>
class DrawingHandler {
public:
	DrawingHandler(Toolbox&);
	~DrawingHandler();

	void update(Toolbox&);

	size_t num_drawers();

private:
	void draw_(Toolbox&);

	bool in_wave_window_(const Toolbox&, const Pevent&) const;

	std::array<float, 2> drawerCoordinates_(const Pevent&, const Toolbox& tb);

	std::list<std::pair<Drawer, SDL_FingerID> > drawpairs_; /*Drawers and their drawing finger*/
	int previous_mstate_ = static_cast<int>(MSTATE::PLACE); /*Keep track of when mstate changes*/

	float draw_value_ = 0.0;
	float erase_value_ = 0.999;

	/*Drawing infrastructure*/
	GLuint vao_;
	GLuint vbo_;
	GLuint fbo_;
	GLuint fbo_wave_;
	Shader shader_draw_;
};