#pragma once
#include <list>
#include <block.hpp>
#include <toolbox.hpp>
#include <enums.hpp>
class BlockchainHandler {
public:
	BlockchainHandler(MSTATE initial_mstate)
		: previous_mstate_(static_cast<int>(initial_mstate)),
		blockchain_(0),
		dragpairs_(0)
	{}

	void update(Toolbox& tb);

	size_t num_blocks();

private:
	void clear_blocks_();
	void update_blocks_(const Toolbox& tb, bool reload_all);

	int previous_mstate_ = static_cast<int>(MSTATE::PLACE); /*Keep track of when mstate changes*/
	std::list<Block> blockchain_; /*Blocks present in system*/
	std::list<std::pair<Block*, SDL_FingerID> > dragpairs_; /*Dragged Blocks and their dragging finger*/
};