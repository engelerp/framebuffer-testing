#pragma once
#include <toolbox.hpp>
#include <string>
#include <glad/glad.h>
#include <shader.hpp>
class PatternHandler {
public:
	PatternHandler() = delete;
	PatternHandler(const PatternHandler&) = delete;
	PatternHandler(Toolbox& tb);

	void update(Toolbox& tb);

private:
	bool load_damping_texture_(const Toolbox& tb, const std::string file, GLuint* texture_target);

	/*Rendering*/
	GLuint vao_ = 0;
	GLuint vbo_ = 0;
	Shader shader_;
	GLuint fbo_static_ = 0;
	GLuint queued_texture_ = 0;
	
	/*Textures*/
	GLuint texture_singleslit_ = 0;
	GLuint texture_doubleslit_ = 0;
	GLuint texture_lattice_ = 0;
	GLuint texture_waveguide_ = 0;
	GLuint texture_ssh_ = 0;
	GLuint texture_fresnel_ = 0;
};