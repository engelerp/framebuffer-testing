#pragma once
#include <toolbox.hpp>
#include <chrono>

class TimeoutHandler {
public:
	TimeoutHandler(int seconds_to_timeout);

	void update(Toolbox& tb);

private:
	int seconds_to_timeout_ = 600;
	std::chrono::time_point<std::chrono::high_resolution_clock> time_last_input_;
};