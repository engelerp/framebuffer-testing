#ifndef INIT_HPP_INCLUDED
#define INIT_HPP_INCLUDED
#include <glad/glad.h>
//#include <SDL2/SDL.h>
#include <SDL.h>
#include <infrastructure.hpp>
#include <string>

Infrastructure Init(const std::string name, const int width, const int height);

#endif
