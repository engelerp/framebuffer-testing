#pragma once
#include <array>
#include <pevent.hpp>
#include <toolbox.hpp>
#include <string>

class EventLogger {
public:
	EventLogger();
	EventLogger(const EventLogger& o) = default;

	void push_events(Toolbox&);

	//DOWN, UP, MOVE, OTHER
	unsigned i_down=0, i_up=0, i_move=0, i_other=0;
	std::array<Pevent, 16> events_down;
	std::array<Pevent, 16> events_up;
	std::array<Pevent, 16> events_move;
	std::array<Pevent, 16> events_other;
};

std::string ptos(const Pevent&, const Toolbox& tb);