#pragma once
#include <enums.hpp>
#include <variant>

struct Message {
	Message(){} //This is needed for something. Slightly scary. TODO: Figure out who needs this.
	Message(BLOCKCHAINMESSAGE message): target(MESSAGETARGET::BLOCKCHAIN), message(message), handled(false) {}
	Message(DRAWERMESSAGE message) : target(MESSAGETARGET::DRAWER), message(message), handled(false) {}
	Message(GUIMESSAGE message) : target(MESSAGETARGET::GUI), message(message), handled(false) {}
	Message(WAVEMESSAGE message) : target(MESSAGETARGET::WAVE), message(message), handled(false) {}
	Message(PATTERNMESSAGE message) : target(MESSAGETARGET::PATTERN), message(message), handled(false) {}

	MESSAGETARGET target;
	std::variant<BLOCKCHAINMESSAGE, DRAWERMESSAGE, GUIMESSAGE, WAVEMESSAGE, PATTERNMESSAGE> message;
	bool handled;
};