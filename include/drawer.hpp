#pragma once
#include <vector>
#include <glad/glad.h>
#include <toolbox.hpp>
class Drawer {
public:
	Drawer(Toolbox& tb);
	Drawer(const Drawer&) = default;
	~Drawer() = default;
	void start_drawing(const float x, const float y);
	/*It is assumed that the caller takes care of binding FBO and Shader*/
	bool draw(const float x, const float y, Toolbox& tb, bool drawing);
	void redraw(Toolbox& tb); //draw last calculated points again
	/*TODO: This can be the same as draw, just add a flag to draw()*/
	void erase(const float x, const float y, Toolbox& tb);
	int num_drawn() const;

private:
	/*Calculate the vertices corresponding to latest drawing step*/
	bool calculate_points_(Toolbox& tb, bool drawing);

	float x0_, y0_;
	float x1_, y1_;
	int num_drawn_;
	std::vector<float> points_;
};