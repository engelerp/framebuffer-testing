#pragma once
#include <SDL.h>
#include <enums.hpp>

struct Pevent {
	Pevent() {} //This is needed for something. Slightly scary. TODO: Figure out who needs this.
	Pevent(const SDL_Event& e)
		: event(e), itcoord_x(0), itcoord_y(0), fscoord_x(0), fscoord_y(0)
	{
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			type = PEVENTTYPE::DOWN;
			finger_id = -1;
			break;
		case SDL_MOUSEBUTTONUP:
			type = PEVENTTYPE::UP;
			finger_id = -1;
			break;
		case SDL_MOUSEMOTION:
			type = PEVENTTYPE::MOVE;
			finger_id = -1;
			break;
		case SDL_FINGERDOWN:
			type = PEVENTTYPE::DOWN;
			finger_id = e.tfinger.fingerId;
			break;
		case SDL_FINGERUP:
			type = PEVENTTYPE::UP;
			finger_id = e.tfinger.fingerId;
			break;
		case SDL_FINGERMOTION:
			type = PEVENTTYPE::MOVE;
			finger_id = e.tfinger.fingerId;
			break;
		default:
			type = PEVENTTYPE::OTHER;
			finger_id = -42;
			break;
		}
	}
	/*SDL_Event that is represented. Type is one of
			SDL_MOUSEBUTTONDOWN
			SDL_MOUSEBUTTONUP
			SDL_MOUSEMOTION
			SDL_FINGERDOWN
			SDL_FINGERUP
			SDL_FINGERMOTION
		*/
	SDL_Event event;
	/*Integer texture coordinates*/
	unsigned itcoord_x;
	unsigned itcoord_y;
	/*Floating point screen coordinates in [0,1]*/
	float fscoord_x;
	float fscoord_y;
	/*Type of event*/
	PEVENTTYPE type;
	/*ID of device that caused event (finger ID)*/
	SDL_FingerID finger_id;
};

class PeventFactory {
public:
	PeventFactory(const int screen_w, const int screen_h, 
								const int texture_w, const int texture_h, 
								const int texoffset_left, const int texoffset_right,
								const int texoffset_bottom, const int texoffset_top)
		:	screen_w_(screen_w), screen_h_(screen_h), texture_w_(texture_w), texture_h_(texture_h),
			texoffset_left_(texoffset_left), texoffset_right_(texoffset_right),
			texoffset_bottom_(texoffset_bottom), texoffset_top_(texoffset_top)
	{}
	Pevent operator()(const SDL_Event&) const;
private:
	int screen_w_, screen_h_; //screen width and height in pixels
	int texture_w_, texture_h_; //texture width and height in pixels
	int texoffset_left_, texoffset_right_, texoffset_bottom_, texoffset_top_; //texture offset (number of pixels off screen per side)
};