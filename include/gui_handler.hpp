#pragma once
#include <toolbox.hpp>
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>
#include <event_logger.hpp>
#include <glad/glad.h>
#include <string>
class GuiHandler {
public:
	GuiHandler(float gui_pos): gui_pos_(gui_pos) {}

	/*Needs fully initialized Infrastructure*/
	void init(Toolbox& tb, const std::string path_img, const std::string path_ttf);

	void update(Toolbox& tb);

	void render(Toolbox& tb);

private:
	bool isInGuiWindow_(const Pevent&) const;
	void draw_gui_(Toolbox& tb);
	void draw_gui_ft_(Toolbox& tb);
	void draw_old_gui_(Toolbox& tb);
	void load_button_textures_(const std::string path);
	void load_image_to_texture_(const std::string file, GLuint& texture);

	float gui_pos_; /*Portion of screen allocated to GUI*/
	/*Potentially we need to do something else here*/
	ImGuiIO io_; /*io*/
	SDL_Event next_event_ = {}; /*Next SDL_Event to take place*/
	SDL_FingerID fingerID_ = -42; /*Finger currently controlling GUI*/
	SDL_TouchID deviceID_ = -1; /*Touchdevice that is controlling GUI*/
	/*For SDL_FINGERDOWN and SDL_FINGERUP we need some special treatment, it's a 2-step procedure*/
	bool lowering_finger_ = false;
	bool raising_finger_ = false;

	/*Button Textures*/
	GLuint btex_immediate_off_ = 0, btex_immediate_on_ = 0;
	GLuint btex_draw_off_ = 0, btex_draw_on_ = 0;
	GLuint btex_erase_off_ = 0, btex_erase_on_ = 0;
	GLuint btex_singleslit_off_ = 0;
	GLuint btex_doubleslit_off_ = 0;
	GLuint btex_lattice_off_ = 0;
	GLuint btex_waveguide_off_ = 0;
	GLuint btex_ssh_off_ = 0;
	GLuint btex_fresnel_off_ = 0;

	/*Font*/
	ImFont* font_ = NULL;

#ifndef NDEBUG
	/*Event Logger for debugging*/
	EventLogger evlog = {};
#endif
};